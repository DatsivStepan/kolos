<?php
class ControllerCommonDashboard extends Controller {
	public function index() {
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));
                $data['adminGroupId'] = $this->user->getGroupId();
		
                $data['heading_title'] = $this->language->get('heading_title');

		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_map'] = $this->language->get('text_map');
		$data['text_activity'] = $this->language->get('text_activity');
		$data['text_recent'] = $this->language->get('text_recent');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		// Check install directory exists
		if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
			$data['error_install'] = $this->language->get('error_install');
		} else {
			$data['error_install'] = '';
		}

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['order'] = $this->load->controller('dashboard/order');
		$data['sale'] = $this->load->controller('dashboard/sale');
		$data['customer'] = $this->load->controller('dashboard/customer');
		$data['online'] = $this->load->controller('dashboard/online');
		$data['map'] = $this->load->controller('dashboard/map');
        $data['map_ukraine'] = $this->url->link('catalog/mapuk', 'token=' . $this->session->data['token'], 'SSL');
		$data['chart'] = $this->load->controller('dashboard/chart');
		$data['activity'] = $this->load->controller('dashboard/activity');
		$data['recent'] = $this->load->controller('dashboard/recent');
		$data['footer'] = $this->load->controller('common/footer');

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');

			$this->model_localisation_currency->refresh();
		}
		//stats
		$this->load->language('common/stats');
		$data['text_social_networks'] = $this->language->get('text_social_networks');
		$data['text_search_sites']    = $this->language->get('text_search_sites');
		$data['text_r_advertising']   = $this->language->get('text_r_advertising');
		$data['text_friends']         = $this->language->get('text_friends');
		$data['text_none']            = $this->language->get('text_none');

		$this->load->model('sale/order');

		$order_total = $this->model_sale_order->getTotalOrdersStatus();
		
		$social_networks = $this->model_sale_order->getTotalOrdersStatus(array('referral' => '1'));
		if ($social_networks) {
			$data['social_networks'] = round(($social_networks / $order_total) * 100);
		} else {
			$data['social_networks'] = 0;
		}
		
		$search_sites = $this->model_sale_order->getTotalOrdersStatus(array('referral' => '2'));
		if ($search_sites) {
			$data['search_sites'] = round(($search_sites / $order_total) * 100);
		} else {
			$data['search_sites'] = 0;
		}
		
		$r_advertising = $this->model_sale_order->getTotalOrdersStatus(array('referral' => '3'));
		if ($r_advertising) {
			$data['r_advertising'] = round(($r_advertising / $order_total) * 100);
		} else {
			$data['r_advertising'] = 0;
		}
		
		$friends = $this->model_sale_order->getTotalOrdersStatus(array('referral' => '4'));
		if ($friends) {
			$data['friends'] = round(($friends / $order_total) * 100);
		} else {
			$data['friends'] = 0;
		}
		
		$data['none'] = 100-$data['friends']-$data['r_advertising']-$data['search_sites']-$data['social_networks'];



		

		

		
		
		//stats

		$this->response->setOutput($this->load->view('common/dashboard.tpl', $data));
	}
}