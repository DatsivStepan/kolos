<?php

class ControllerCatalogMapuk extends Controller
{
    private $error = array();

    public function index()
    {

        $this->document->setTitle($this->language->get(''));

        $data['map'] = $this->load->controller('dashboard/map_ukraine');
        
      $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('Карта України'),
            'href' => $this->url->link('catalog/mapuk', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['heading_title'] = 'Карта України';
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('catalog/mapuk.tpl', $data));
    }
	

}