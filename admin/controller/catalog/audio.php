<?php

class ControllerCatalogAudio extends Controller
{
    private $error = array();

    public function index()
    {

        $this->document->setTitle($this->language->get('Фонове аудіо'));

        $this->load->model('review/audio');

        $this->getList();
    }


    protected function getList()
    {
		  if ( isset ( $_POST["send"] ) ) {
              $direct =DIR_IMAGE.'/audio/';
	if (( $_FILES["upfile"]["size"] < 5024*5024*2 )&&($_FILES["upfile"]["type"] == "audio/mp3")) {

		if ( is_uploaded_file ( $_FILES["upfile"]["tmp_name"] ) ) {
			if ( move_uploaded_file ( $_FILES["upfile"]["tmp_name"], $direct.$_FILES["upfile"]["name"] ) ) {
				$filename=$_FILES["upfile"]["name"];
				$fileurl=$direct.$_FILES["upfile"]["name"];
				$results = $this->model_review_audio->addAudio($filename,$fileurl);
			} 
		} 
	}

} 
	      if ( isset ( $_POST["add"] ) ) {
	  if ( isset ( $this->request->post["id"] ) ) {
				$id=$this->request->post["id"];
				$data['id'] = $id;
				$results = $this->model_review_audio->addBackAudio($id);
	  }
}
          if ( isset ( $_POST["voll"] ) ) {
            if ( isset ( $this->request->post["voll"] ) ) {
                $id=$this->request->post["vollli"];
                $id=  $id/1000;
                $results = $this->model_review_audio->addAudioVoll($id);
            }
        }
          if ( isset ( $_POST["delete"] ) ) {
	           
			   if ( isset ( $this->request->post["id"] ) ) {
				$id=$this->request->post["id"];
				$resultsdell = $this->model_review_audio->getAudioD($id);
                $results = $this->model_review_audio->dellAudio($resultsdell['0']['id']);
				if(file_exists($resultsdell['0']['url'])){
				unlink($resultsdell['0']['url']);

				}}
} 
       
	   $curentsound = $this->model_review_audio->curAudio();
	   if($curentsound){
	   $data['curentsound'] = $curentsound['0']['name'];
	   }else{
		  $data['curentsound'] = 'не встановлене'; 
	   }
        
        $gych = $this->model_review_audio->curAudioGych();
        $data['gych'] = $gych['0']['volume'];

	   
      $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('Фонове аудіо'),
            'href' => $this->url->link('catalog/audio', 'token=' . $this->session->data['token'], 'SSL')
        );
	   
	   
        $data['audio'] = array();

        $results = $this->model_review_audio->getAudio();
        if($results){
            foreach ($results as $result) {
                $data['audio'][] = array(
                    'id' => $result['id'],
                    'name' => $result['name'],
                    'status' => $result['status']
                );
            }
        }else{
            $results=FALSE;
        }


		
		if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
		
        $data['heading_title'] = 'Аудіо';
		$data['action'] = $this->url->link('catalog/audio', 'token=' . $this->session->data['token']);
        $data['token'] = $this->session->data['token'];
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('catalog/audio.tpl', $data));
    }
	

}