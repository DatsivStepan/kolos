<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

$_['text_complete_status']   = 'Завершені замовлення'; 
$_['text_processing_status'] = 'Замовлення в обробці'; 
$_['text_other_status']      = 'Інші статуси'; 

$_['text_social_networks']   = 'Соціальні мережі'; 
$_['text_search_sites']      = 'Пошукові сайти'; 
$_['text_r_advertising']     = 'Реклама'; 
$_['text_friends']           = 'Друзі'; 
$_['text_none']              = 'Не вказані'; 