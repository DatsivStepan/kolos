<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Фонове аудіо</h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            

              <div class="form-group">
			  <div class="col-sm-3">
			  
			  
                <label class="control-label" for="input-date-added">Виберіть файл</label>
                <div class="input-group date">
				<form action = "<?php echo $action; ?>" method = 'post' enctype='multipart/form-data'>
				 
				  <input type ='file' name = 'upfile'/>
                  
                  
			      <button style="    margin-top: 25px;" type="submit" name='send' id="button-filter" class="btn btn-primary pull-right"> Завантажити</button>
                  </form>
				 </div>
              </div>
              <div class="col-sm-3">
			  
			  
                <label class="control-label" for="input-date-added">Виберіть пісню</label>
                <div class="input-group date">
				<form action = "<?php echo $action; ?>" method = 'post' enctype='multipart/form-data'>
				    <select name="id"  class="form-control">
                      <?php foreach ($audio as $audios) { ?>
                      <option value="<?php echo $audios['id']; ?>"><?php echo $audios['name']; ?></option>
                      <?php } ?>
                    </select>
                  
                  
			      <button style="    margin-top: 25px;" type="submit" name='delete' id="button-filter" class="btn btn-danger pull-right"> Видалити</button>
                  </form>
				 </div>
              </div>
			  <div class="col-sm-3">
			  
			  
                <label class="control-label" for="input-date-added">Поточне аудіо:</label>
				<label class="control-label" for="input-date-added"><?php echo $curentsound; ?></label>
				
                <div class="input-group date">
				<form action = "<?php echo $action; ?>" method = 'post' enctype='multipart/form-data'>
				  <select name="id"  class="form-control">
                      <?php foreach ($audio as $audios) { ?>
                      <option value="<?php echo $audios['id']; ?>"><?php echo $audios['name']; ?></option>
                      <?php } ?>
                    </select>
                  
                  
			      <button style="    margin-top: 25px;" type="submit" name='add' id="button-filter" class="btn btn-success pull-right"> Встановити на фон</button>
                  </form>
				 </div>
              </div>
                  <div class="col-sm-3">
                      <label class="control-label" for="input-date-added">Поточна гучність = <?php echo $gych*1000; ?> </label>
                      <label class="control-label" for="input-date-added">Гучність аудіо мах 10000 min 0</label>
                      <div class="input-group date">
                          <form action = "<?php echo $action; ?>" method = 'post' enctype='multipart/form-data'>

                              <input type ='numder' class="form-control" name = 'vollli'/>


                              <button style="    margin-top: 25px;" type="submit" name='voll' id="button-filter" class="btn btn-primary pull-right"> Зберегти</button>
                          </form>
                      </div>
                  </div>
            </div>

          </div>
        </div>
	
  
      </div>

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <td style="display: none;" class="text-center"></td>
                <td class="text-center">Назва</td>
                <td class="text-center">Статус</td>
                <td class="text-center">Дії</td>
            </tr>
            </thead>
            <tbody>
            <?php if ($audio) { ?>
            <?php foreach ($audio as $audios) { ?>
            <form action = "<?php echo $action; ?>" method = 'post' enctype='multipart/form-data'>
            <tr>
                <td class="text-center" style="display: none;"><input name ='id' style="display: none;" value = "<?php echo $audios['id']; ?>" /></td>
                <td class="text-center"><?php echo $audios['name'];?> </td>
                <td class="text-center"><?php
                 if($audios['status']==1){echo 'Встановлено';}else{echo 'не встановлено';};?> </td>
                <td class="text-center">
                    <button style="    margin-top: 25px;" type="submit" name='delete' id="button-filter" class="btn btn-danger pull-right"> Видалити</button>
                    <button style="    margin-top: 25px;" type="submit" name='add' id="button-filter" class="btn btn-success pull-right"> Встановити на фон</button>

                </td>
            </tr></form>
            <?php } ?>
            <?php } else { ?>
            <tr>
                <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>
  </div>
 </div>
<?php echo $footer; ?>