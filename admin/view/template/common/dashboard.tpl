<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_install) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $order; ?></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $sale; ?></div>
      <?php if($adminGroupId != 10){ ?>
        <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $customer; ?></div>
        <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $online; ?></div>
      <?php } ?>
    </div>
	<div class="row">
	<div id="stats" style="display: block; background: #455E96;">
  <ul>
    <li>
      <div><?php echo $text_social_networks; ?> <span class="pull-right"><?php echo $social_networks; ?>%</span></div>
      <div class="progress" style="height: 13px;">
        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $social_networks; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $social_networks; ?>%"> <span class="sr-only"><?php echo $social_networks; ?>%</span> </div>
      </div>
    </li>
    <li>
      <div><?php echo $text_search_sites; ?> <span class="pull-right"><?php echo $search_sites; ?>%</span></div>
      <div class="progress" style="height: 13px;">
        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $search_sites; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $search_sites; ?>%"> <span class="sr-only"><?php echo $search_sites; ?>%</span> </div>
      </div>
    </li>
    <li>
      <div><?php echo $text_r_advertising; ?> <span class="pull-right"><?php echo $r_advertising; ?>%</span></div>
      <div class="progress" style="height: 13px;">
        <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $r_advertising; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $r_advertising; ?>%"> <span class="sr-only"><?php echo $r_advertising; ?>%</span> </div>
      </div>
    </li>
	  <li>
      <div><?php echo $text_friends; ?> <span class="pull-right"><?php echo $friends; ?>%</span></div>
      <div class="progress" style="height: 13px;">
        <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $friends; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $friends; ?>%"> <span class="sr-only"><?php echo $friends; ?>%</span> </div>
      </div>
    </li>
		  <li>
      <div><?php echo $text_none; ?> <span class="pull-right"><?php echo $none; ?>%</span></div>
      <div class="progress" style="height: 13px;">
        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $none; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $none; ?>%"> <span class="sr-only"><?php echo $none; ?>%</span> </div>
      </div>
    </li>
  </ul>
</div>
	</div>
    <div class="row">
      <div class="col-lg-6 col-md-12 col-sx-12 col-sm-12"><?php echo $map; ?></div>
      <div class="col-lg-6 col-md-12 col-sx-12 col-sm-12"><?php echo $chart; ?></div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sx-12 col-sm-12 text-center"><h4>
      <a href="<?php echo $map_ukraine; ?>" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="Перегляд"> Доставка по Україні</a></h4></div>
      
    </div>

    <div class="row">
      <?php if($adminGroupId != 10){ ?>
        <div class="col-lg-4 col-md-12 col-sm-12 col-sx-12"><?php echo $activity; ?></div>
      <?php } ?>
      <div class="col-lg-8 col-md-12 col-sm-12 col-sx-12"> <?php echo $recent; ?> </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>