<?php

class ModelReviewZauv extends Model
{


    public function getZauvs($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "zauv WHERE id !=0 ";

        $sqt = "UPDATE " . DB_PREFIX . "zauv SET `stan` = '1' WHERE id !=0 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND InputName LIKE '" . $this->db->escape($data['filter_product']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $sql .= " AND InputEmail LIKE '" . $this->db->escape($data['filter_author']) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND stan = '" . (int)$data['filter_status'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        $sort_data = array(
            'InputName',
            'status',
            'date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY r.date_added";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        $queryt = $this->db->query($sqt);

        return $query->rows;
    }

    public function getTotalZauvs($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zauv WHERE id > 0";

        if (!empty($data['filter_name'])) {
            $sql .= " AND InputName LIKE '" . $this->db->escape($data['filter_product']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $sql .= " AND InputEmail LIKE '" . $this->db->escape($data['filter_author']) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND stan = '" . (int)$data['filter_status'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalZauvNew()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zauv  WHERE stan = '0' ");

        return $query->row['total'];
    }

}