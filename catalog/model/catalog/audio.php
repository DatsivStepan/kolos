<?php
class ModelCatalogAudio extends Model {
 public function getAudio()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "audio WHERE status =1";

        $query = $this->db->query($sql);

        return $query->rows;
    }
    public function getAudioVol()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "audiovolume";

        $query = $this->db->query($sql);

        return $query->rows;
    }
}