<?php
// Text
$_['cart_items']     = ' item(s)';
$_['cartt']     = 'Your cart';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';
$_['button_clear'] = 'Empty';