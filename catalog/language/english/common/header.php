<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['delivery_header_ukraine']           = 'Delivery of goods to Ukraine';
$_['header_profile_text']           = 'Profile';
$_['header_info_1']           = 'We work around the clock';
$_['header_info_2']           = 'Book product';
$_['header_info_3']           = 'Pay<br />viable option';
$_['header_info_4']           = 'Get the goods<br />from the courier';
$_['kozak']           = 'image/kozak_en.png';
$_['kozakkat']           = 'image/kozakkat_en.png';

$_['review']           = 'Reviews';

$_['inform']           = 'Information';


