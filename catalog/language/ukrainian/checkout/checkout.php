<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']                  = 'Підтвердити замовлення';
$_['button_reg']                     = 'вхід / реєстрація';

// Text
$_['text_cart']                      = 'Кошик';
$_['text_checkout_option']           = 'Крок 1: Варіанти оформлення';
$_['text_checkout_account']          = 'Крок 2: Обліковий запис і деталі рахунку';
$_['text_checkout_payment_address']  = 'Крок 2: Деталі рахунку';
$_['text_checkout_shipping_address'] = 'Крок 3: Деталі доставки';
$_['text_checkout_shipping_method']  = 'Крок 4: Спосіб доставки';
$_['text_checkout_payment_method']   = 'Крок 5: Спосіб оплати';
$_['text_checkout_confirm']          = 'Крок 6: Підтвердження замовлення';
$_['text_modify']                    = 'Змінити;';
$_['text_new_customer']              = 'Новий покупець';
$_['text_returning_customer']        = 'Постійний покупець';
$_['text_checkout']                  = 'Опції оформлення замовлення:';
$_['text_i_am_returning_customer']   = 'Я постійний покупець';
$_['text_register']                  = 'Зареєструвати профіль';
$_['text_guest']                     = 'Оформлення замовлення без реєстрації';
$_['text_register_account']          = 'Після створення профілю Ви зможете робити покупки швидше, відслідковувати статус замовлення і зберігати історію попередніх замовлень.';
$_['text_forgotten']                 = 'Забули пароль?';
$_['text_your_details']              = 'Ваша особиста інформація';
$_['text_your_address']              = 'Ваша адреса';
$_['text_your_password']             = 'Ваш пароль';
$_['text_agree']                     = 'Я прочитав і погоджуюся з <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Я хочу використати нову адресу';
$_['text_address_existing']          = 'Я хочу використати існуючу адресу';
$_['text_shipping_method']           = 'Будь-ласка вкажіть спосіб доставки, який Ви обрали для цього замовлення.';
$_['text_payment_method']            = 'Будь-ласка вкажіть спосіб оплати, який Ви обрали для цього замовлення.';
$_['text_comments']                  = 'Додати коментарі до замовлення';
$_['text_recurring']                 = 'Повторювний товар';
$_['text_payment_recurring']           = 'Профіль оплати';
$_['text_trial_description']         = '%s кожен %d %s(s) до %d платіж(ів) тоді';
$_['text_payment_description']       = '%s кожен %d %s(s) до %d платіж(ів)';
$_['text_payment_until_canceled_description'] = '%s кожен %d %s(s) поки не скасовано';
$_['text_day']                       = 'день';
$_['text_week']                      = 'тиждень';
$_['text_semi_month']                = 'половину місяця';
$_['text_month']                     = 'місяць';
$_['text_year']                      = 'рік';

// Column
$_['column_name']                    = 'Назва товару';
$_['column_model']                   = 'Модель';
$_['column_quantity']                = 'Кількість';
$_['column_price']                   = 'Ціна за одиницю';
$_['column_total']                   = 'Загалом';

// Entry
$_['entry_email_address']            = 'E-Mail одержувача';
$_['entry_email']                    = 'E-Mail одержувача';
$_['entry_password']                 = 'Пароль';
$_['entry_confirm']                  = 'Підтвердження паролю';
$_['entry_firstname']                = 'Ім`я та прізвище одержувача';
$_['entry_lastname']                 = 'По батькові одержувача';
$_['entry_telephone']                = 'Телефон одержувача';
$_['entry_fax']                      = 'Факс';
$_['entry_address']                  = 'Оберіть адресу';
$_['entry_company']                  = 'Компанія';
$_['entry_customer_group']           = 'Група покупців';
$_['entry_address_1']                = 'Адреса одержувача';
$_['entry_address_2']                = 'Адреса 2';
$_['entry_postcode']                 = 'Індекс';
$_['entry_city']                     = 'Місто';
$_['entry_country']                  = 'Країна';
$_['entry_zone']                     = 'Область одержувача';
$_['entry_newsletter']               = 'Я хочу підписатися на розсилку %s новин.';
$_['entry_shipping'] 	             = 'Моя адреса доставки і оплати співпадають.';

// Error
$_['error_warning']                  = 'В процесі обробки Вашого замовлення виникли проблеми! Якщо проблеми не зникнуть попробуйте обрати інший метод оплати або пишіть у <a href="%s">службу пітримки</a>.';
$_['error_login']                    = 'E-Mail і/чи пароль не співпадають.';
$_['error_approved']                 = 'Ваш профіль потребує активації перед тим як Ви зможете ввійти.';
$_['error_exists']                   = 'Цей E-Mail вже зареєтровано!';
$_['error_firstname']                = 'Ім`я повинно містити від 1 до 32 символів!';
$_['error_lastname']                 = 'Прізвище повинно містити від 1 до 32 символів!';
$_['error_email']                    = 'Неправильний E-Mail!';
$_['error_telephone']                = 'Телефон повинен містити від 3 до 32 символів!';
$_['error_password']                 = 'Пароль повинен містити від 3 до 20 символів!';
$_['error_confirm']                  = 'Підтвердження паролю і пароль не співпадають!';
$_['error_address_1']                = 'Адреса 1 повинна містити від 3 до 128 символів!';
$_['error_city']                     = 'Назва міста повинна містити від 2 до 128 символів!';
$_['error_postcode']                 = 'Індес повинен містити від 2 до 10 символів!';
$_['error_country']                  = 'Оберіть країну!';
$_['error_zone']                     = 'ОБеріть Район/Область!';
$_['error_agree']                    = 'Ви мусите погодитися з %s!';
$_['error_address']                  = 'Ви мусите обрати адресу!';
$_['error_shipping']                 = 'Оберіть спосіб доставки!';
$_['error_no_shipping']              = 'Немає варіантів доставки. Будь-ласка <a href="%s">зв`яжіться з нами</a> для допомоги!';
$_['error_payment']                  = 'Оберіть спосіб оплати!';
$_['error_no_payment']               = 'Немає варіантів оплати. Будь-ласка <a href="%s">зв`яжіться з нами</a> для допомоги!';
$_['error_custom_field']             = '%s необхідно!';

$_['cart_cost_text']                 = 'Вартість';
$_['cart_delivery_text']             = 'Доставка';
$_['cart_commission_text']           = 'Комісія';
$_['cart_number_of_products_text']   = 'Кількість товарів';
$_['cart_weight_text']               = 'Вага';
$_['cart_the_total_cost_text']       = 'Загальна вартість';
$_['parcel_count_text']              = 'Кількість посилок';


$_['v_cart']             = 'В кошик';
$_['parcel_count_text_pos']             = 'Посилки(ка)';

$_['osob']               = 'Особливі відмітки';
$_['iak']                = 'Як ви дізналися про нас';
$_['bad1']               = 'Проблеми із здоровям ';
$_['bad2']               = 'Погано чує ';
$_['bad3']               = 'Погано бачить ';
$_['bad4']               = 'Не ходячий ';
$_['bad5']               = 'Інші вади ';
$_['iak1']               = 'Соціальні мережі';
$_['iak2']               = 'Пошукові сайти';
$_['iak3']               = 'Реклама';
$_['iak4']               = 'Друзі';
$_['type_oplatu']        = 'Тип оплати';

$_['korzuna'] = 'В корзину';

