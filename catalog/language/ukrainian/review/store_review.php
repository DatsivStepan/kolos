<?php
// Text
$_['heading_title']	= 'Відгуки';

$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';

// Entry
$_['entry_name']               = "Ваше Ім'я";
$_['entry_review']             = 'Ваш Відгук';
$_['entry_mail']               = 'Ваш Mail';
$_['entry_rating']             = 'Оцінка';
$_['entry_good']               = 'Добре';
$_['entry_bad']                = 'Погано';
$_['send']                     = 'Залишити відгук';

// Помилка
$_['error_name']    = "Попередження: Ім'я повинно бути від 3 до 25 символів!";
$_['error_mail']    = 'Увага: Mail';
$_['error_text']    = 'Попередження: Текст відгуку повинен бути від 25 до 3000 символів!';
$_['error_rating']  = 'Увага: Будь ласка, виберіть оцінку огляду';
$_['error_captcha'] = 'Увага: Код перевірки не збігається із зображенням!';

