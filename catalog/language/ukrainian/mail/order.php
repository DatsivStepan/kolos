<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_new_subject']          = '%s - Замовлення %s';
$_['text_new_greeting']         = 'Шановний покупець! Ваше замовлення прийняте до виконання .';
$_['text_new_received']         = 'Ви отримали замовлення.';
$_['text_new_link']             = 'Для перегляду замовлення натисніть на наступне посилання:';
$_['text_new_order_detail']     = 'Деталі замовлення';
$_['text_new_instruction']      = 'Інструкції';
$_['text_new_order_id']         = 'Термін доставки 3 - 7 днів. Номер замовлення:';
$_['text_new_date_added']       = 'Дата створення:';
$_['text_new_order_status']     = 'Статус замовлення:';
$_['text_new_payment_method']   = 'Спосіб оплати:';
$_['text_new_shipping_method']  = 'Спосіб доставки:';
$_['text_new_email']  			= 'E-mail:';
$_['text_new_telephone']  		= 'Телефон:';
$_['text_new_ip']  				= 'IP Адреса:';
$_['text_new_payment_address']  = 'Адреса оплати';
$_['text_new_shipping_address'] = 'Адреса доставки';
$_['text_new_products']         = 'Товари';
$_['text_new_product']          = 'Товар';
$_['text_new_model']            = 'Модель';
$_['text_new_quantity']         = 'Кількість';
$_['text_new_price']            = 'Ціна';
$_['text_new_order_total']      = 'Замовлення загалом';
$_['text_new_total']            = 'Разом';
$_['text_new_download']         = 'Після підтвердження платежу Ви зможете натиснути наступне посилання для завантаження:';
$_['text_new_comment']          = 'Коментарі до вашого замовлення наступні:';
$_['text_new_footer']           = 'З запитаннями і побажаннями просимо звертатись на  info@kolosexpress.com
Дякуємо за співпрацю, з повагою колектив  Kolosexpress.';
$_['text_update_subject']       = '%s - Замовлення оновлено %s';

$_['text_update_order']         = 'Термін доставки 3 - 7 днів   номер замовлення:';
$_['text_update_date_added']    = 'Дата замовлення:';
$_['text_update_order_status']  = 'Шановний покупець!
Статус Вашого замовлення змінено на:';
$_['text_update_comment']       = 'Коментарі до вашого замовлення наступні:';
$_['text_update_link']          = 'Для перегляду замовлення натисніть наступне посилання:';
$_['text_update_footer']        = 'Щиро дякуємо за співпрацю з повагою Kolos  Express.';
