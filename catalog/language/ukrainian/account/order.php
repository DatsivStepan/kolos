<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']      = 'Історія замовлень';
$_['heading_title1']     = 'Профайл';
$_['heading_zam']        = 'Замовлення';
$_['heading_spov']       = 'Сповіщення';
$_['heading_kyp']        = 'Купони';
$_['save']               = 'Зберегти';
$_['entry_password']     = 'Введіть пароль';
$_['entry_confirm']      = 'Підтвердіть пароль';
$_['show_order']         = 'Переглянути заказ';
$_['decoration']         = 'В обробці';
$_['posted']             = 'Відправлено';
$_['delivered']          = 'Доставлено';
$_['decorated']          = 'Оформлено';
$_['tovar']              = 'Товар';
$_['kilkist']            = 'К-сть';
$_['cina']               = 'Ціна';
$_['vartist']            = 'Вартість';
$_['status']             = 'Статус';
$_['close']              = 'Закрити';
$_['osob']               = 'Особливі відмітки';
$_['iak']                = 'Як ви дізналися про нас';
$_['kilkistt']           = 'Кількість';
$_['kom']                = 'Комісія';
$_['kilkistpos']         = 'Кількість посилок';
$_['zagvat']             = 'Загальна вартість';
$_['more']               = 'Детальніше ';

// Text
$_['text_account']          = 'Обліковий запис';
$_['text_order']            = 'Інформація про замовлення';
$_['text_order_detail']     = 'Деталі замовлення';
$_['text_invoice_no']       = 'Рахунок №:';
$_['text_order_id']         = 'Номер замовлення:';
$_['text_date_added']       = 'Дата створення:';
$_['text_shipping_address'] = 'Адреса доставки';
$_['text_shipping_method']  = 'Метод достави:';
$_['text_payment_address']  = 'Адреса платника';
$_['text_payment_method']   = 'Метод оплати:';
$_['text_comment']          = 'Коментарі до замовлення';
$_['text_history']          = 'Історія замовлень';
$_['text_success']          = 'Ви успішно додали <a href="%s">%s</a> до Вашої <a href="%s">корзини</a>!';
$_['text_empty']            = 'У Вас немає попередніх замовлень!';
$_['text_error']            = 'Немає замовлнення, яке Ви шукаєте!';

// Column
$_['column_order_id']       = 'Номер замовлення';
$_['column_product']        = 'Кількість товарів';
$_['column_customer']       = 'Покупець';
$_['column_name']           = 'Назва товару';
$_['column_model']          = 'Модель';
$_['column_quantity']       = 'Кількість';
$_['column_price']          = 'Ціна';
$_['column_total']          = 'Разом';
$_['column_action']         = 'Дія';
$_['column_date_added']     = 'Дата створення';
$_['column_status']         = 'Статус замовлення';
$_['column_comment']        = 'Коментарі';

// Error
$_['error_reorder']         = '%s зараз недоступний для замовлення.';
$_['bad1']               = 'Проблеми із здоровям ';
$_['bad2']               = 'Погано чує ';
$_['bad3']               = 'Погано бачить ';
$_['bad4']               = 'Не ходячий ';
$_['bad5']               = 'Інші вади ';
$_['iak1']               = 'Соціальні мережі';
$_['iak2']               = 'Пошукові сайти';
$_['iak3']               = 'Реклама';
$_['iak4']               = 'Друзі';