<?php

// Text
$_['cart_items']    = ' товар(ів)';
$_['cartt']    = 'Ваш кошик';
$_['text_empty']    = 'Ваш кошик порожній!';
$_['text_cart']     = 'Переглянути кошик';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring']  = 'Профіль оплати';
$_['button_clear'] = 'Очистити';