<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_home']          = 'Головна';
$_['text_wishlist']      = 'Список побажань (%s)';
$_['text_shopping_cart'] = 'Кошик';
$_['text_category']      = 'Категорії';
$_['text_account']       = 'Обліковий запис';
$_['text_register']      = 'Реєстрація';
$_['text_login']         = 'Вхід';
$_['text_order']         = 'Історія замовлень';
$_['text_transaction']   = 'Оплати';
$_['text_download']      = 'Завантаження';
$_['text_logout']        = 'Вихід';
$_['text_checkout']      = 'Оформлення замовлення';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Переглянути всі';
$_['delivery_header_ukraine']           = 'Доставка товарів по Україні';
$_['header_profile_text']           = 'Профайл';
$_['header_info_1']           = 'Працюємо цілодобово';
$_['header_info_2']           = 'Замовляйте<br/>товар';
$_['header_info_3']           = 'Оплачуйте вигідним<br />варіантом';
$_['header_info_4']           = 'Отримуйте товар<br />від кур`єра';
$_['kozak']           = 'image/kozak.png';
$_['kozakkat']           = 'image/kozakkat.png';
$_['review']           = 'Відгуки';

$_['inform']           = 'Інформація';
