<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login'] = 'Вход';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';
$_['delivery_header_ukraine']           = 'Доставка товаров по Украине';
$_['header_profile_text']           = 'Профайл';
$_['header_info_1']           = 'Работаем круглосуточно';
$_['header_info_2']           = 'Заказывайте<br />товар';
$_['header_info_3']           = 'Оплачивайте<br />выгодным вариантом';
$_['header_info_4']           = 'Получайте товар<br />от курьера';
$_['kozak']           = 'image/kozak_ru.png';
$_['kozakkat']           = 'image/kozakkat_ru.png';
$_['review']           = 'Отзывы';
