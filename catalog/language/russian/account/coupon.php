<?php
// Heading
$_['heading_title']      = 'Учетная запись';
$_['heading_title1']     = 'Профайл';
$_['heading_zam']        = 'Заказ';
$_['heading_spov']       = 'Уведомление';
$_['heading_kyp']        = 'Купоны';
$_['save']               = 'Сохранить';
$_['entry_password']     = 'Введите пароль';
$_['entry_confirm']      = 'Подтвердите пароль';


// Text
$_['text_account']       = 'Личный Кабинет';
$_['text_edit']          = 'Редактировать информацию';
$_['text_your_details']  = 'Ваша учетная запись';
$_['text_success']       = 'Ваша учетная запись была успешно обновлена!';

// Entry
$_['entry_firstname']    = 'Имя, Отчество';
$_['entry_lastname']     = 'Фамилия';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Телефон';
$_['entry_fax']          = 'Факс';

// Error
$_['error_exists']       = 'Данный E-Mail уже зарегистрирован!';
$_['error_firstname']    = 'Имя должно быть от 1 до 32 символов!';
$_['error_lastname']     = 'Фамилия должна быть от 1 до 32 символов!';
$_['error_email']        = 'E-Mail адрес введен неверно!';
$_['error_telephone']    = 'Номер телефона должен быть от 3 до 32 символов!';
$_['error_custom_field'] = '%s необходим!';
