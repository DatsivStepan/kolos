<?php
// Heading
$_['heading_title']      = 'История заказов';
$_['heading_title1']     = 'Профайл';
$_['heading_zam']        = 'Заказ';
$_['heading_spov']       = 'Уведомление';
$_['heading_kyp']        = 'Купоны';
$_['save']               = 'Сохранить';
$_['entry_password']     = 'Введите пароль';
$_['entry_confirm']      = 'Подтвердите пароль';
$_['show_order']         = 'Посмотреть заказ';
$_['decoration']         = 'В обработке';
$_['posted']             = 'Отправлено';
$_['delivered']          = 'Доставлено';
$_['decorated']          = 'Оформлены';
$_['tovar']              = 'Товар';
$_['kilkist']            = 'К-во';
$_['cina']               = 'Цена';
$_['vartist']            = 'Стоимость';
$_['status']             = 'Статус';
$_['close']              = 'Закрыть';
$_['osob']               = 'Особые отметки';
$_['iak']                = 'Как вы узнали о нас';
$_['kilkistt']           = 'Количество';
$_['kom']                = 'Комиссия';
$_['kilkistpos']         = 'Количество посылок';
$_['zagvat']             = 'Общая стоимость ';
$_['more']               = 'Подробнее ';

// Text
$_['text_account']          = 'Личный Кабинет';
$_['text_order']            = 'Заказ';
$_['text_order_detail']     = 'Детали заказа';
$_['text_invoice_no']       = '№ Счета';
$_['text_order_id']         = '№ Заказа';
$_['text_date_added']       = 'Добавлено';
$_['text_shipping_address'] = 'Адрес доставки';
$_['text_shipping_method']  = 'Способ доставки';
$_['text_payment_address']  = 'Платёжный адрес';
$_['text_payment_method']   = 'Способ оплаты';
$_['text_comment']          = 'Комментарий к заказу';
$_['text_history']          = 'История заказов';
$_['text_success']          = 'Товары из заказа <a href="%s">%s</a> успешно добавлены <a href="%s">в вашу корзину</a>!';
$_['text_empty']            = 'Вы еще не совершали покупок!';
$_['text_error']            = 'Запрошенный заказ не найден!';

// Column
$_['column_order_id']       = '№ Заказа';
$_['column_product']        = '№ Товара';
$_['column_customer']       = 'Клиент';
$_['column_name']           = 'Название товара';
$_['column_model']          = 'Модель';
$_['column_quantity']       = 'Количество';
$_['column_price']          = 'Цена';
$_['column_total']          = 'Всего';
$_['column_action']         = 'Действие';
$_['column_date_added']     = 'Добавлено';
$_['column_status']         = 'Статус';
$_['column_comment']        = 'Комментарий';

// Error
$_['error_reorder']         = '%s в данный момент не доступен....';

$_['bad1'] = 'Проблемы со здоровьем';
$_['bad2'] = 'Плохо слышит';
$_['bad3'] = 'Плохо видит';
$_['bad4'] = 'Не ходятящий';
$_['bad5'] = 'Другие недостатки';
$_['iak1'] = 'Социальные сети';
$_['iak2'] = 'Поисковые сайты';
$_['iak3'] = 'Реклама';
$_['iak4'] = 'Друзья';
