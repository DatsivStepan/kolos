<?php
class ControllerModuleBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}


			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
			$data['category_id'] = $category_info['name'];
		} else {
			$product_id = $this->request->get['product_id'];
			$category_info_all = $this->model_catalog_product->getCategories($product_id);
			if ($category_info_all) {
				$category_id_s = $category_info_all['0']['category_id'];
				$category_info_name = $this->model_catalog_category->getCategory($category_id_s);

				if ($category_info_name) {
					$category_info['name'] = $category_info_name['name'];
				}
			} else {
				$category_info['name'] = 'нема';
			}


		}
		$a = 0;
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				if ($result['title'] == $category_info['name']) {
					$data['banners'][] = array(
						'title' => $result['title'],
						'link' => $result['link'],
						'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
					);
					$a = 1;
				}
			}
		}
		$data['akcii'] = $a;
		$data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/banner.tpl', $data);
		} else {
			return $this->load->view('default/template/module/banner.tpl', $data);
		}

	}
}