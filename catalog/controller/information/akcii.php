<?php

class ControllerInformationAkcii extends Controller
{
	public function index() {
		$this->load->language('information/information');
		$data['acii'] = $this->language->get('acii');
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
     $data['base'] = $this->config->get('config_ssl');
  } else {
     $data['base'] = $this->config->get('config_url');
  }
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		//start

		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');


		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner(9);


		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link' => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], '848', '200')
				);

			}
		}
		$data['module'] = $module++;


		//end

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/akcii.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/akcii.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/akcii.tpl', $data));
		}
	}
}