<?php

class ControllerInformationZauvajennia extends Controller
{
    public function index()
    {
        $this->load->language('information/information');
        $data['zauv'] = $this->language->get('zauv');
        $data['name'] = $this->language->get('name');
        $data['mail'] = $this->language->get('mail');
        $data['tema'] = $this->language->get('tema');
        $data['text'] = $this->language->get('text');
        $data['send'] = $this->language->get('send');
        $data['corect'] = $this->language->get('corect');
        $data['erore'] = $this->language->get('erore');
        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $data['base'] = $this->config->get('config_ssl');
        } else {
            $data['base'] = $this->config->get('config_url');
        }
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink(HTTP_SERVER, 'canonical');
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');



        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            if ((utf8_strlen($this->request->post['InputName']) < 3) || (utf8_strlen($this->request->post['InputName']) > 50)) {
                $data['error'] = 'Прізвище';
            }

            if ((utf8_strlen($this->request->post['InputEmail']) < 3) || (utf8_strlen($this->request->post['InputEmail']) > 50)) {
                $data['error'] = 'Email';
            }

            if ((utf8_strlen($this->request->post['InputReal']) < 3) || (utf8_strlen($this->request->post['InputReal']) > 30)) {
                $data['error'] = 'тема';
            }

            if ((utf8_strlen($this->request->post['InputMessage']) < 3) || (utf8_strlen($this->request->post['InputMessage']) > 3000)) {
                $data['error'] = 'текст';
            }

            if (!isset($data['error'])) {
                $InputName = $this->request->post['InputName'];
                $InputEmail = $this->request->post['InputEmail'];
                if (!isset($this->request->post['InputReal'])) {
                    $InputReal = ' ';
                } else {
                    $InputReal = $this->request->post['InputReal'];
                }

                $InputMessage = $this->request->post['InputMessage'];
                $this->load->model('review/zauv');

                $zaput = $this->model_review_zauv->addZauv($InputName, $InputEmail, $InputReal, $InputMessage);
                if (!isset($zaput)) {
                    $data['stan'] = '1';
                } else {
                    $data['stan'] = '0';
                }
            } else {
                $data['stan'] = '0!';
            }
        } else {
            $data['stan'] = '2';
        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/zauvajennia.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/zauvajennia.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/common/zauvajennia.tpl', $data));
        }
    }


}