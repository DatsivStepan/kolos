<?php
class ControllerPaymentPhoneCheckout extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/phone_checkout.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/phone_checkout.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/phone_checkout.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'phone_checkout') {
            $this->load->language('payment/phone_checkout');
			$this->load->model('checkout/order');

            $json = array();

            if (isset($this->request->post['telephone-p']) && ((utf8_strlen($this->request->post['telephone-p']) < 3) || (utf8_strlen($this->request->post['telephone-p']) > 32))) {
                $json['error']['telephone-p'] = $this->language->get('error_telephone');
            }else{
                $this->session->data['phone_payment_number'] = $this->request->post['telephone-p'];
                $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('phone_checkout_order_status_id'), $this->request->post['telephone-p']);
                $json['success'] = $this->url->link('checkout/success');
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
		}
	}
}