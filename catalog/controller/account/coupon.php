<?php
class ControllerAccountCoupon extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/edit', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/coupon');

		$data['heading_title1'] = $this->language->get('heading_title1');
		$data['heading_zam'] = $this->language->get('heading_zam');
		$data['heading_spov'] = $this->language->get('heading_spov');
		$data['heading_kyp'] = $this->language->get('heading_kyp');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('account/coupon');
		$this->load->model('account/notification');

		// if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		// 	$this->model_account_customer->editCustomer($this->request->post);
		//
		// 	$this->session->data['success'] = $this->language->get('text_success');
		//
		// 	// Add to activity log
		// 	$this->load->model('account/activity');
		//
		// 	$activity_data = array(
		// 		'customer_id' => $this->customer->getId(),
		// 		'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
		// 	);
		//
		// 	$this->model_account_activity->addActivity('edit', $activity_data);
		//
		// 	$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		// }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_coupon'),
			'href'      => $this->url->link('account/coupon', '', 'SSL')
		);

		$data['heading_title'] = 'КУПОНИ';

		// $data['text_your_details'] = $this->language->get('text_your_details');
		// $data['text_additional'] = $this->language->get('text_additional');
		// $data['text_select'] = $this->language->get('text_select');
		// $data['text_loading'] = $this->language->get('text_loading');
		//
		// $data['entry_firstname'] = $this->language->get('entry_firstname');
		// $data['entry_lastname'] = $this->language->get('entry_lastname');
		// $data['entry_email'] = $this->language->get('entry_email');
		// $data['entry_telephone'] = $this->language->get('entry_telephone');
		// $data['entry_fax'] = $this->language->get('entry_fax');
		//
		// $data['button_continue'] = $this->language->get('button_continue');
		// $data['button_back'] = $this->language->get('button_back');
		// $data['button_upload'] = $this->language->get('button_upload');
		// $data['entry_password'] = $this->language->get('entry_password');
		// $data['entry_confirm'] = $this->language->get('entry_confirm');
		//
		// if (isset($this->error['warning'])) {
		// 	$data['error_warning'] = $this->error['warning'];
		// } else {
		// 	$data['error_warning'] = '';
		// }
		//
		// if (isset($this->error['firstname'])) {
		// 	$data['error_firstname'] = $this->error['firstname'];
		// } else {
		// 	$data['error_firstname'] = '';
		// }
		//
		// if (isset($this->error['lastname'])) {
		// 	$data['error_lastname'] = $this->error['lastname'];
		// } else {
		// 	$data['error_lastname'] = '';
		// }
		//
		// if (isset($this->error['email'])) {
		// 	$data['error_email'] = $this->error['email'];
		// } else {
		// 	$data['error_email'] = '';
		// }
		//
		// if (isset($this->error['telephone'])) {
		// 	$data['error_telephone'] = $this->error['telephone'];
		// } else {
		// 	$data['error_telephone'] = '';
		// }
		//
		// if (isset($this->error['password'])) {
		// 	$data['error_password'] = $this->error['password'];
		// } else {
		// 	$data['error_password'] = '';
		// }
		//
		// if (isset($this->error['confirm'])) {
		// 	$data['error_confirm'] = $this->error['confirm'];
		// } else {
		// 	$data['error_confirm'] = '';
		// }
		//
		// if (isset($this->error['custom_field'])) {
		// 	$data['error_custom_field'] = $this->error['custom_field'];
		// } else {
		// 	$data['error_custom_field'] = array();
		// }
		//
		// $data['action'] = $this->url->link('account/edit', '', 'SSL');

		// if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$coupons = $this->model_account_coupon->getCoupons();
			$data['coupons'] = $coupons;
		// }
		$data['notifications_count'] = $this->model_account_notification->getNotificationsCount($this->customer->getId());

		$data['back'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/coupon.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/coupon.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/coupon.tpl', $data));
		}
	}
}
