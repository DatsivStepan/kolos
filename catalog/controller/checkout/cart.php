<?php
class ControllerCheckoutCart extends Controller {
    public function index() {
        $this->document->addScript('catalog/view/javascript/cart.js');
        $this->load->language('checkout/cart');
        if (isset($this->request->get['remove_all'])) {
            $this->cart->clear();
        }
        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'href' => $this->url->link('common/home'),
            'text' => $this->language->get('text_home')
        );

        $data['breadcrumbs'][] = array(
            'href' => $this->url->link('checkout/cart'),
            'text' => $this->language->get('heading_title')
        );
        
        $data['cart_cost_text'] = $this->language->get('cart_cost_text');
        $data['cart_delivery_text'] = $this->language->get('cart_delivery_text');
        $data['cart_commission_text'] = $this->language->get('cart_commission_text');
        $data['cart_number_of_products_text'] = $this->language->get('cart_number_of_products_text');
        $data['cart_weight_text'] = $this->language->get('cart_weight_text');
        $data['cart_the_total_cost_text'] = $this->language->get('cart_the_total_cost_text');
        $data['parcel_count_text'] = $this->language->get('parcel_count_text');

        if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
            
            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_recurring_item'] = $this->language->get('text_recurring_item');
            $data['text_next'] = $this->language->get('text_next');
            $data['text_next_choice'] = $this->language->get('text_next_choice');

            $data['column_image'] = $this->language->get('column_image');
            $data['column_name'] = $this->language->get('column_name');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $data['button_update'] = $this->language->get('button_update');
            $data['button_remove'] = $this->language->get('button_remove');
            $data['button_shopping'] = $this->language->get('button_shopping');
            $data['button_checkout'] = $this->language->get('button_checkout');

            if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                $data['error_warning'] = $this->language->get('error_stock');
            } elseif (isset($this->session->data['error'])) {
                $data['error_warning'] = $this->session->data['error'];

                unset($this->session->data['error']);
            } else {
                $data['error_warning'] = '';
            }

            if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
                $data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
            } else {
                $data['attention'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            $data['action'] = $this->url->link('checkout/cart/edit', '', true);
            
            if ($this->config->get('config_cart_weight')) {
                $data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
            } else {
                $data['weight'] = '';
            }
            //var_dump($data['weight']);exit;
            $this->load->model('tool/image');
            $this->load->model('tool/upload');

            $data['products'] = array();
            $data['count'] = 0;

            $products = $this->cart->getProducts();

            $result = file_get_contents("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=3");

            $currency_usd = $this->currency->getValue('USD');
            foreach (json_decode($result, true) as $currency){
                if($currency["ccy"] == "USD" && $currency["base_ccy"] == "UAH") {
                    $currency_usd = $currency['buy'];
                    break;
                }
            }

            $data['curency_usd'] = 0;


            foreach ($products as $product) {
                $product_total = 0;

                foreach ($products as $product_2) {
                    if ($product_2['product_id'] == $product['product_id']) {
                        $product_total += $product_2['quantity'];
                    }
                }

                if ($product['minimum'] > $product_total) {
                    $data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
                }

                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                } else {
                    $image = '';
                }

                $option_data = array();

                foreach ($product['option'] as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name'  => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
                } else {
                    $total = false;
                }

                $recurring = '';

                if ($product['recurring']) {
                    $frequencies = array(
                        'day'        => $this->language->get('text_day'),
                        'week'       => $this->language->get('text_week'),
                        'semi_month' => $this->language->get('text_semi_month'),
                        'month'      => $this->language->get('text_month'),
                        'year'       => $this->language->get('text_year'),
                    );

                    if ($product['recurring']['trial']) {
                        $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
                    }

                    if ($product['recurring']['duration']) {
                        $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                    } else {
                        $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                    }
                }
                $product_weight = $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));

                $data['count'] += $product['quantity'];
                $data['products'][] = array(
                    'key'       => $product['key'],
                    'product_id'       => $product['product_id'],
                    'thumb'     => $image,
                    'name'      => $product['name'],
                    'product_weight'      => $product_weight,
                    'weight'    => $this->weight->format($product_weight, $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point')),
                    'model'     => $product['model'],
                    'option'    => $option_data,
                    'recurring' => $recurring,
                    'quantity'  => $product['quantity'],
                    'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                    'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                    'price'     => $price,
                    'total'     => $total,
                    'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                );

                $usd_price = $this->currency->format(($price / $currency_usd), '','',false);

                $data['curency_usd'] += $usd_price * $product['quantity'];
            }
            //var_dump($data['products']);exit;
            // Gift Voucher
            $data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $key => $voucher) {
                    $data['vouchers'][] = array(
                        'key'         => $key,
                        'description' => $voucher['description'],
                        'amount'      => $this->currency->format($voucher['amount']),
                        'remove'      => $this->url->link('checkout/cart', 'remove=' . $key)
                    );
                }
            }

            $data['delivery'] = $this->currency->format($this->config->get('flat_cost'));


            $count_parcel = ceil($this->cart->getWeight()/30);
            $parcel = explode("-", $this->config->get('config_parcel'));
            $data['parcel'] = [];
            $total_parcel = 0;
            for($i = 1; $i<=$count_parcel; $i++){
                if($i == 1) {
                    $total_parcel += $parcel[0];
                    $data['parcel'][] = array(
                        'price' => $parcel[0].' $',
                        'count' => $i
                    );
                }else{
                    $total_parcel += $parcel[1];
                    $data['parcel'][] = array(
                        'price' => $parcel[1].' $',
                        'count' => $i
                    );
                }
            }

            $data['curency_usd'] += $this->currency->format($total_parcel, '','',false);

            // Totals
            $this->load->model('extension/extension');

            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);

                        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    }
                }

                $sort_order = array();

                foreach ($total_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $total_data);
            }

            $data['totals'] = array();
            $totalPrice =  $this->cart->getTotal();
            $coupon_tmp = 0;

            foreach ($total_data as $total) {
                switch($total['code']){
                    case 'coupon': {
                        $coupon_tmp = $total['value'];
                        break;
                    }
                    case 'total': {
                        $totalPrice = $total['value'];
                        break;
                    }
                }
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text'  => $this->currency->format($total['value'])
                );
            }

            $data['curency_usd'] += $this->currency->format(($coupon_tmp/ $currency_usd), '','',false);

            $commission = explode(";", $this->config->get('config_commision'));

            $data['commission'] = 0;
            $totalPriceUsd=$totalPrice/$currency_usd;
            foreach ($commission as $key => $el) {
                $tmp_el = explode("-", $el);
                if (count($tmp_el) == 1) {
                    $val_tmp = explode(':', reset($tmp_el));
                    if ($totalPriceUsd < reset($val_tmp)) {
                        
                        $data['commission'] = end($val_tmp);
                    }
                    continue;
                }
                if (count($tmp_el) == 2) {
                    $val_price = explode(':', $el)[1];
                    $val_tmp = explode('-', explode(':', $el)[0]);
                    if ($totalPriceUsd >= reset($val_tmp) && $totalPriceUsd < end($val_tmp)) {
                        $data['commission'] = $val_price;
                    }
                }
            }

            if(!next($commission)){
                $max_commission = explode(':', explode('-', end($commission))[1]);

                if($max_commission[0] < $totalPrice){
                    $data['commission'] = $max_commission[1];
                }
            }
            $this->load->language('checkout/coupon');

            if (array_key_exists("coupontype", $this->session->data)) {
				if ($this->session->data['coupontype']==''){
                    $this->session->data['coupontype']='0';
                }
			}else{
				$this->session->data['coupontype']='0';
			}
			
				if($this->session->data['coupontype']=='K'){
                $data['commission'] = 0;
            }
		
            

            $data['curency_usd'] += $this->currency->format(($data['commission']), '','',false);
            $data['curency_usd'] .= " $";
            $totalPriceAll = (float)$totalPrice + ($data['commission'] * $currency_usd) + (int)$this->config->get('flat_cost') + ($total_parcel * $currency_usd);

            $data['totalPrice'] = $this->currency->format($totalPriceAll);
            $data['totalPriceUsd'] = $this->currency->format($totalPriceUsd);
            
			
			if($this->session->data['coupontype']=='Z'){
                $timeout = 5; 
                $timeout = $timeout * 60;
     if (isset($_SESSION['start_time'])) {
    $elapsed_time = time() - $_SESSION['start_time'];
    if ($elapsed_time >= $timeout) {
        $_SESSION['coupontype']='';
        unset($_SESSION['start_time']);
    }
           
                }else{ $_SESSION['start_time'] = time(); }
                $data['totalPrice'] = 0;
                $data['curency_usd'] = '0 $';
            }
            $data['continue'] = $this->url->link('common/home');

            $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

            $this->load->model('extension/extension');

            $data['checkout_buttons'] = array();

            $data['coupon'] = $this->load->controller('checkout/coupon');
            $data['voucher'] = $this->load->controller('checkout/voucher');
            $data['reward'] = $this->load->controller('checkout/reward');
            $data['shipping'] = $this->load->controller('checkout/shipping');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/cart.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/checkout/cart.tpl', $data));
            }
        } else {
            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_error'] = $this->language->get('text_empty');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            unset($this->session->data['success']);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }
    }

    public function add() {
        $this->load->language('checkout/cart');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = (int)$this->request->post['product_id'];
        } else {
            $product_id = 0;
        }
        if (isset($this->request->post['statusmusic'])) {
            $_SESSION['statusmusic'] = $this->request->post['statusmusic'];
        }
        if (isset($this->request->post['startmusic'])) {
            $_SESSION['startmusic'] = $this->request->post['startmusic'];
        }
        if (isset($this->request->post['volmusic'])) {
            $_SESSION['volmusic'] = $this->request->post['volmusic'];
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
                $quantity = (int)$this->request->post['quantity'];
            } else {
                $quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
            }

            
            if (isset($this->request->post['option'])) {
                $option = array_filter($this->request->post['option']);
            } else {
                $option = array();
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                }
            }

            if (isset($this->request->post['recurring_id'])) {
                $recurring_id = $this->request->post['recurring_id'];
            } else {
                $recurring_id = 0;
            }

            $recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

            if ($recurrings) {
                $recurring_ids = array();

                foreach ($recurrings as $recurring) {
                    $recurring_ids[] = $recurring['recurring_id'];
                }

                if (!in_array($recurring_id, $recurring_ids)) {
                    $json['error']['recurring'] = $this->language->get('error_recurring_required');
                }
            }

            if (!$json) {
                $this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);
                
                if ($this->config->get('config_cart_weight')) {
                    $weight = $this->cart->getWeight();
                } else {
                    $weight = '';
                }
                $this->load->language('checkout/cart');
                $json['text1'] = $this->language->get('text1');;
                $json['text2'] = $this->language->get('text2');;
                $json['text3'] = $this->language->get('text3');;
                $json['weight'] = $weight;
                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);

                // Totals
                $this->load->model('extension/extension');

                $total_data = array();
                $total = 0;
                $taxes = $this->cart->getTaxes();

                // Display prices
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $sort_order = array();

                    $results = $this->model_extension_extension->getExtensions('total');

                    foreach ($results as $key => $value) {
                        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                    }

                    array_multisort($sort_order, SORT_ASC, $results);

                    foreach ($results as $result) {
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('total/' . $result['code']);

                            $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                        }
                    }

                    $sort_order = array();

                    foreach ($total_data as $key => $value) {
                        $sort_order[$key] = $value['sort_order'];
                    }

                    array_multisort($sort_order, SORT_ASC, $total_data);
                }

                $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
            } else {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function ajaxEdit() {
        $this->load->language('checkout/cart');

        $json = array();

        // Update
        if (!empty($this->request->post['quantity'])) {
            $quantity = $this->request->post['quantity'];
            $key = $this->request->post['key'];

            $this->cart->update($key, $quantity);


            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);


            // Totals
            $this->load->model('extension/extension');

            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);

                        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    }
                }

                $sort_order = array();

                foreach ($total_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }



                array_multisort($sort_order, SORT_ASC, $total_data);
            }

            $data['totals'] = array();
            $coupon_tmp = 0;
            $totalPrice = $this->cart->getTotal();
            foreach ($total_data as $total) {
                switch($total['code']){
                    case 'coupon': {
                        $coupon_tmp = $total['value'];
                        break;
                    }
                    case 'total': {
                        $totalPrice = $total['value'];
                        break;
                    }
                }
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text'  => $this->currency->format($total['value'])
                );
            }

            if ($this->config->get('config_cart_weight')) {
                $count_weight = $this->cart->getWeight();
                $data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
            } else {
                $data['weight'] = '';
            }

            $count = 0;
            $product_weight = 0;
            $products = $this->cart->getProducts();

            $result = file_get_contents("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=3");

            $currency_usd = $this->currency->getValue('USD');
            foreach (json_decode($result, true) as $currency){
                if($currency["ccy"] == "USD" && $currency["base_ccy"] == "UAH") {
                    $currency_usd = $currency['buy'];
                    break;
                }
            }

            $currency_usd_data = 0;


            foreach ($products as $product) {
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                $usd_price = $this->currency->format(($price / $currency_usd), '','',false);

                $currency_usd_data += $usd_price * $product['quantity'];
            }

            $currency_usd_data += $this->currency->format(($coupon_tmp/ $currency_usd), '','',false);

            $products = $this->cart->getProduct($key, $quantity);
            foreach($products as $product) {
                $count += $product['quantity'];
            }

            //var_dump($products);


            $p_weight = $this->weight->convert($products[$this->request->post['key']]['weight'], $products[$this->request->post['key']]['weight_class_id'], $this->config->get('config_weight_class_id'));
            $product_weight = $this->weight->format($p_weight, $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));

            $commission = explode(";", $this->config->get('config_commision'));

            $data['commission'] = 0;
            $totalPriceUsd=$totalPrice/$currency_usd;
            foreach ($commission as $key => $el) {
                $tmp_el = explode("-", $el);
                if (count($tmp_el) == 1) {
                    $val_tmp = explode(':', reset($tmp_el));

                    if ($totalPriceUsd < reset($val_tmp)) {
                        $data['commission'] = end($val_tmp);
                    }
                    continue;
                }
                if (count($tmp_el) == 2) {
                    $val_price = explode(':', $el)[1];
                    $val_tmp = explode('-', explode(':', $el)[0]);

                    if ($totalPriceUsd >= reset($val_tmp) && $totalPriceUsd < end($val_tmp)) {
                        $data['commission'] = $val_price;
                    }
                }

                if(!next($commission)){
                    $max_commission = explode(':', explode('-', end($commission))[1]);

                    if($max_commission[0] < $totalPrice){
                        $data['commission'] = $max_commission[1];
                    }
                }
            }

            $currency_usd_data += $this->currency->format(($data['commission']), '','',false);

            //var_dump(explode(':', end(explode('-', end($commission))))[0]);


            $parcel = explode("-", $this->config->get('config_parcel'));
            $count_parcel = ceil($this->cart->getWeight()/30);
            $data['parcel'] = [];
            $parcel_str = '';
            $total_parcel = 0;
            for($i = 1; $i<=$count_parcel; $i++){
                if($i == 1) {
                    $total_parcel += $parcel[0];
                    $parcel_str .= "<div class=\"row\">
                                    <span>".$i."-".$parcel[0].' $'."</span>
                                </div>";
                }else{
                    $total_parcel += $parcel[1];
                    $parcel_str .= "<div class=\"row\">
                                    <span>".$i."-".$parcel[1].' $'."</span>
                                </div>";
                }
            }

            $currency_usd_data += $this->currency->format(($total_parcel), '','',false);

            $totalPriceAll = (float)$totalPrice + ($data['commission'] * $currency_usd) + (int)$this->config->get('flat_cost') + ($total_parcel * $currency_usd);

            $data['totalPrice'] = $this->currency->format($totalPriceAll);


            $this->response->addHeader('Content-Type: application/json');

            //$this->response->setOutput(json_encode($this->session->data['cart'][$this->request->post['key']]));
            $this->response->setOutput(json_encode(["product" => $this->cart->getProduct($key, $quantity), "count" => $count, "totals" => $data['totals'], 'weight' => $data['weight'], "totalPrice" => $data['totalPrice'].'/'.$currency_usd_data.' $', "commission" => $data['commission'], "p_w" => $product_weight, "parcel" => $parcel_str]));

            //$this->response->redirect($this->url->link('checkout/cart'));
        }
    }


    public function edit() {
        $this->load->language('checkout/cart');

        $json = array();

        // Update
        if (!empty($this->request->post['quantity'])) {
            foreach ($this->request->post['quantity'] as $key => $value) {
                $this->cart->update($key, $value);
            }

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);

            $this->response->redirect($this->url->link('checkout/cart'));
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function remove() {
        $this->load->language('checkout/cart');

        $json = array();

        // Remove
        if (isset($this->request->post['key'])) {
            $this->cart->remove($this->request->post['key']);

            unset($this->session->data['vouchers'][$this->request->post['key']]);

            $this->session->data['success'] = $this->language->get('text_remove');

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);

            // Totals
            $this->load->model('extension/extension');

            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);

                        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    }
                }

                $sort_order = array();

                foreach ($total_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $total_data);
            }
            if ($this->config->get('config_cart_weight')) {
                    $weight = $this->cart->getWeight();
                } else {
                    $weight = '';
                }
                
                $json['weight'] = $weight;
            
            $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}