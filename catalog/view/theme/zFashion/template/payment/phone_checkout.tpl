<form>
    <div class="row" id="phone_payment">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="">Телефон</label>
                <input type="tel" class="form-control" name="telephone-p">
            </div>
        </div>
    </div>
</form>
<div class="buttons">
    <div class="pull-left">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary"
               data-loading-text="<?php echo $text_loading; ?>"/>
    </div>
</div>
<script type="text/javascript"><!--
    $('#button-confirm').bind('click', function () {
        $.ajax({
            type: 'post',
            url: 'index.php?route=payment/phone_checkout/confirm',
            data: $('#phone_payment :input'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-confirm').button('loading');
            },
            complete: function () {
                $('#button-confirm').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['error']) {
                    for (i in json['error']) {
                        $('#phone_payment input[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                    }
                }

                if (json['success']) {
                    location = json['success'];
                }
            }
        });
    });
    //--></script>
