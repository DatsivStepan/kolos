<?php echo $header; ?>
<div class="container">
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8 col-md-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>">
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
			<?php if ($products) { ?>
			<div class="filter-form">
				<div class="form-group sort-by">
					<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
					<select name="sort" id="input-sort" class="form-control">
						<?php foreach ($sorts as $sorts) { ?>
							<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
								<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
							<?php } else { ?>
								<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				<div class="form-group price-limits">
					<label class="control-label" for="input-sort"><?php echo $text_price; ?>:</label>
					<input name="min" type="text" class="form-control"/
					placeholder=' <?php echo $prise_min; ?>'> :
					<input name="max" type="text" class="form-control"/
					placeholder=' <?php echo $prise_max; ?>'>
				</div>
				<div class="form-group producer">
					<label class="control-label" for="input-sort"> <?php echo $text_manufacturer; ?> </label>
					<select name="vurobnik" style="display: inline-block; width: 65%;" id="input-sort" class="form-control">
						<option value="" elected="selected"> </option>
						<?php
						$style='';
						foreach ($manufacture as $manufactures) { ?>
						<?php if ($manufactures['name'] == $man) { ?>
						<option value="<?php echo $manufactures['href']; ?>"
								selected="selected"><?php echo $manufactures['name']; ?></option>
						<?php } else { ?>
								<option value="<?php echo $manufactures['href']; ?>"><?php echo $manufactures['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
				<button id="save-submit" class="btn filter-button" onclick="myFunction()"> <?php echo $text_refine; ?> </button>
			</div>
			<h3 class="text-center product-name-list"><?php echo $heading_title; ?></h3>
			<div class="row">
				<?php $z=4; $q=0; foreach ($products as $product) {   ?>
				<?php if($q%$z==0){ ?>
			</div>
			<div class="row">
				<?php }$q++; ?>
					<div class="product-id-<?=$product['product_id'];?> col-sm-6 col-md-4 col-lg-3 product-item" <?=$style; ?> >
							<?php if ($product['special']) { ?>
					<span class="ribbon">HOT<b></b></span>
				<?php } ?>
						<div class="product-thumb transition">
							<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
							<div class="image">
								<a href="<?php echo $product['href']; ?>">
									<img src="<?php echo $product['thumb']; ?>"
										 alt="<?php echo $product['name']; ?>"
										 title="<?php echo $product['name']; ?>"
										 class="img-responsive"/>
								</a>
							</div>
							<div class="caption">
								<div class="row no-margin text-center">
									<div class="col-xs-12">
										<?php if ($product['price']) { ?>
										<p class="price">
											<?php if (!$product['special']) { ?>
											<?php echo $product['price']; ?>
											<?php } else { ?>
											<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
											<?php } ?>
										</p>
										<?php } ?>
									</div>
									<div class="col-xs-12">
										<i class="charges fa fa-minus minusQuantityCount"></i>
										<input type="text" name="quantity" value="<?=$product['minimum']?>" size="1"  class="text-center input-quantity" />
										<i class="charges fa fa-plus plusQuantityCount"></i>
									</div>
								</div>
								<div class="row add-block">
									<div class="purse">
									</div>
									<button type="button" class="btn button_cart" onclick="cart.add('<?php echo $product['product_id']; ?>', $('.product-id-<?=$product['product_id'];?>').find('.input-quantity').val())" > <?php echo $button_cart; ?></button>
								</div>
							</div>
						</div>
					</div>
					<?php if($q==8){   ?>
					<?php } ?>
				<?php }  ?>
			</div>
			<div class="pagination-wrap">
				<hr>
				<div class="row">
					<div class="col-sm-6 count-product"><?php echo $results; ?></div>
					<div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
				</div>
			</div>
			<?php } ?>
			<?php if (!$categories && !$products) { ?>
				<div class="row">
					<div class="col-md-3">
						<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
						<select name="sort" style="display: inline-block; width: 50%;" id="input-sort" class="form-control">
							<?php foreach ($sorts as $sorts) { ?>
							<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
							<option value="<?php echo $sorts['href']; ?>"
									selected="selected"><?php echo $sorts['text']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-3">
						<label class="control-label" for="input-sort">Ціна:</label>
						<input name="min" style="display: inline-block; width: 35%;" type="text" class="form-control"/
						value='<?php echo $prise_min; ?>'>:
						<input name="max" style="display: inline-block; width: 35%;" type="text" class="form-control"/
						value='<?php echo $prise_max; ?>'>
					</div>
					<div class="col-md-3">
						<label class="control-label" for="input-sort">Виробник</label>
						<select name="vurobnik" style="display: inline-block; width: 65%;" id="input-sort" class="form-control">
							<option value="" elected="selected">виберіть виробника</option>
							<?php foreach ($manufacture as $manufactures) { ?>
							<?php if ($manufactures['name'] == $man) { ?>
							<option value="<?php echo $manufactures['href']; ?>"
									selected="selected"><?php echo $manufactures['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $manufactures['href']; ?>"><?php echo $manufactures['name']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-3">
						<button id="save-submit"
								style="color: #ffffff; display: inline-block; width: 100%; background-color: #ff0000;"
								class="btn button_cart" onclick="myFunction()">Фільтрувати
						</button>
					</div>
				</div>
				<p><?php echo $text_empty; ?></p>
			<?php } ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script>
	function myFunction() {
		var baseUrl = '<?= $hreff; ?>';


		var prise_min = $('input[name=min]').val();
		var prise_max = $('input[name=max]').val();
		var sort = $('select[name=sort]').val();
		var vurob = $('select[name=vurobnik]').val();


		if (prise_min) {
			baseUrl = baseUrl + '&prise_min=' + prise_min;
		}

		if (prise_max) {
			baseUrl = baseUrl + '&prise_max=' + prise_max;
		}

		if (vurob) {
			baseUrl = baseUrl + vurob;
		}


		location = baseUrl + sort;

	}
</script>
