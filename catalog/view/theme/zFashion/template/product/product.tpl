<?php echo $header; ?>
<div class="container">
    <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
        <!-- <h3 class="product-name-page"><?php// echo $heading_title; ?></h3> -->
        <div class="product-wrapper">
        <div class="col-sm-6">
            <?php if ($thumb || $images) { ?>
                <ul class="thumbnails text-center">
                    <?php if ($thumb) { ?>
                        <li style="overflow-y: hidden;">
                            <a class="product-img" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                <img class="prodimggolov" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                <div class="row text-right no-margin" style="margin-top: -11%;">
                                    <span class="zoom-product-img glyphicon glyphicon-zoom-in pull-right"></span>
                                </div>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($images) { ?>
                        <div id="style-2" class="row thumbnail scrollbar divscrol" >
                            <?php foreach ($images as $image) { ?>
                                <li class="image-additional">
                                    <a class="" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                                        <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                    </a>
                                </li>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>

        <div class="col-sm-6 karttovar">
            <span id="spanprod" class="product_span" style="opacity: 0;"><?php echo $heading_title; ?></span>
            <h3 class=" text-center orangeproduct" onmouseover="document.getElementById('spanprod').style.opacity = '1';" onmouseout="document.getElementById('spanprod').style.opacity = '0';"><?php echo $heading_title_slov; ?></h3>
            <div class="row">
                <div class="col-sm-push-7 col-sm-5 tovarback">
                    <img class="prodimg" src="../image/tovar.png" alt=""/>
                </div>
                <div class="col-sm-pull-5 col-sm-7 karttovar-left text-center">
                    <div class="tovarpad text-left">
                        <a target="_blank" href="https://vk.com"><img src="../image/vk.png" alt=""/></a>
                        <a target="_blank" href="https://twitter.com"><img src="../image/tv.png" alt=""/></a>
                        <a target="_blank" href="https://www.facebook.com"><img src="../image/fa.png" alt=""/></a>
                    </div>
                    <div class="tovarpad text-left">
                        <div class="price text-left">
                            <?php if ($price) { echo $pricename.':'; } ?>
                        </div>
                        
                        <?php if (!$special) { ?>
                        <h4 class="font_header_prise">  
              <?php echo $price; ?>
               </h4>
            <?php } else { ?>
           <span style="text-decoration: line-through;"><?php echo $price; ?></span>
           <h4 class="font_header_prise"> 
              <?php echo $pricename.':'; echo $special; ?>
             </h4>
            <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row headerBgBottom text-center">
                <hr>
                <img src="../image/kolosheader.png" alt=""/>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-description">
                    <div id="product">
                        <div class="">
                            <div class="row">
                                <div class="col-sm-6 text-center smleft">

                                    <span class="quont-minus">
                                        <i class="fa fa-minus"></i>
                                    </span>

                                    <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity"
                                           class="input_product text-center"/>

                                    <span class="quont-plus">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                                    <img id="image" class="mishimg" src="image/mishok.png" alt="">

                                </div>
                                <div class="col-sm-6">
                                    <button type="button" id="button-cart"
                                            data-loading-text="<?php echo $text_loading; ?>"
                                            class="btn button_cart button_cart_prod"><?php echo $button_cart; ?></button>
                                </div>


                            </div>


                        </div>
                        <?php if ($minimum > 1) { ?>
                        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                        <?php } ?>
                    </div>

                </div>

            </div>
        </div>
        <div class="clearfix"></div>
            <?php
             if (strlen($description) !== 0)  {
            if ($description !=='<p><br></p>') { ?>
            <div class="col-sm-12 description">
                <h3 class=" text-center orangedescription" ><?php echo $tab_description;  ?></h3>
                <?php echo $description;  ?>
            </div>
            <?php } }?>
      </div>
    </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>





<?php if ($products) { ?>

    <div class="col-sm-offset-4 col-md-offset-3 col-sm-8 col-md-9">

        <h3 class="recommended-product-header"><?php echo $text_related; ?></h3>


        <div id="myCarousel" class="carousel slide" data-ride="carousel" style="display: block;">


            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <?php $q=0; foreach ($products as $product) { ?>
                    <?php if($q<4){ ?>
                    <div class="product-id-<?=$product['product_id'];?> fiveparts">
                        <div class="product-thumb transition">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <div class="image">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>"
                                         alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>"
                                         class="img-responsive"/>
                                </a>
                            </div>
                            <div class="caption">
                                <div class="row no-margin text-center">
                                    <div class="col-sm-12">
                                        <?php if ($product['price']) { ?>
                                        <p class="price">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <span class="price-new"><?php echo $product['special']; ?></span> <span
                                                    class="price-old"><?php echo $product['price']; ?></span>
                                            <?php } ?>
                                        </p>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-12">
                                        <i class="charges fa fa-minus minusQuantityCount"></i>
                                        <input type="text" name="quantity" value="<?=$product['minimum']?>" size="1"
                                               class="text-center input-quantity"/>
                                        <i class="charges fa fa-plus plusQuantityCount"></i>
                                    </div>
                                </div>
                                <div class="row add-block">
                                    <div class="purse">
                                    </div>
                                    <button type="button" class="btn button_cart"
                                        onclick="cart.add('<?php echo $product['product_id']; ?>', $('.product-id-<?=$product['product_id'];?>').find('.input-quantity').val())"> <?php echo $button_cart; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($q>=5){ ?>
                    <?php if($q%5==0){ ?>
                    <div class="clearfix"></div>
                </div>
                <div class="item">
                    <?php } ?>
                    <div class="product-id-<?=$product['product_id'];?> fiveparts">
                        <div class="product-thumb transition">
                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                            <div class="image">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>"
                                         alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>"
                                         class="img-responsive"/>
                                </a>
                            </div>
                            <div class="caption">
                                <div class="row no-margin text-center">
                                    <div class="col-xs-12">
                                        <?php if ($product['price']) { ?>
                                        <p class="price">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <span class="price-new"><?php echo $product['special']; ?></span> <span
                                                    class="price-old"><?php echo $product['price']; ?></span>
                                            <?php } ?>
                                        </p>
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-12">
                                        <i class="charges fa fa-minus minusQuantityCount"></i>
                                        <input type="text" name="quantity" value="<?=$product['minimum']?>" size="1"
                                               class="text-center input-quantity"/>
                                        <i class="charges fa fa-plus plusQuantityCount"></i>
                                    </div>
                                </div>
                                <div class="row add-block">
                                    <div class="purse">

                                    </div>
                                    <button type="button" class="btn button_cart"
                                        onclick="cart.add('<?php echo $product['product_id']; ?>', $('.product-id-<?=$product['product_id'];?>').find('.input-quantity').val())"> <?php echo $button_cart; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php $q++; } ?>
                    <div class="clearfix"></div>
                </div>
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
        </div>


    </div>
<?php } ?>







<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				
				$('#cart > span').html('Ваш кошик<br> ' + json['total']+ '<img src="image/trik.png" alt="" style="    padding-bottom: 10%;">');

                //$('html, body').animate({ scrollTop: 0 }, 'slow');
                var beepOne = $("#beep-one")[0];
                beepOne.play();


                var add = document.getElementById('add').title;
                if (add == 'Українська'){add='Добавлено';}
                    if (add == 'russian'){add='Добавлено';}
                        if (add == 'English'){add='Added';}


                var d = document.getElementById("button-cart");
                d.className += " added_to_cart";
                $("#image").attr("src","image/mishok_cart.png");


                $('#cart > ul').load('index.php?route=common/cart/info ul li');


            }
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<script>
    jQuery(document).ready(function ($) {
        $('.quont-minus').click(function () {
            document.getElementById('input-quantity').value--;
            var input = document.getElementById('input-quantity');

            if (input.value == -1)
                input.value = 0;
        });
        $('.quont-plus').click(function () {

            document.getElementById('input-quantity').value++;
    });
    });
</script>
<?php echo $footer; ?>