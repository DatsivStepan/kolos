<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="row" style="    padding: 30px 0px;">
    <div class="col-sm-3">
       <img src="image/vidgyk.png" class="" alt="">
        <strong><?php echo $review['author']; ?></strong>
    </div>
    <div class="col-sm-9">
        <p><?php echo $review['text']; ?></p>
        <div class="row">
            <div class="col-sm-8 text-left">     
      <?php for ($i = 1; $i <= 5; $i++) { ?>
      <?php if ($review['rating'] < $i) { ?>
      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } else { ?>
      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } ?>
      <?php } ?></div>
            <div class="col-sm-4 text-right"><?php echo $review['date_added']; ?></div>
            
        </div>
    </div>
</div>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
