<?php echo $header; ?>
<?php if (!$logged) { 
                        $_SESSION['stanredirect']=0;
                        $stancom="disabled";
                         }else{ 
                         $stancom=" ";
                         $_SESSION['stanredirect']=1;
                         } 
                         ?>
<div class="container">
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-8 col-md-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<div class="col-md-12">
				<div class="breadcrumbs">
					<ul class="breadcrumb">
						<?php foreach ($breadcrumbs as $breadcrumb) { ?>
						<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>

			<div class="error"></div>
			<div class="col-md-12 checkout_form">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for=""><?php echo $entry_firstname; ?></label>
							<input type="text" class="form-control" name="firstname" <?php echo $stancom; ?>>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for=""><?php echo $entry_lastname; ?> </label>
							<input type="text" class="form-control" name="lastname" <?php echo $stancom; ?>>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for=""><?php echo $entry_zone; ?></label>
						   <?php

							if($lang == 'en'){
							$oblast = [
                            'ОД' => 'Odessa oblast',
							'ДН' => 'Dnipropetrovsk oblast',
							'ЧГ'=>  'Chernihiv oblast',
							'ХА' => 'Kharkiv oblast',
							'ЖТ' => 'Zhytomyr oblast',
							'ПО' => 'Poltava oblast',
							'ХЕ' => 'Kherson oblast',
							'КВ' => 'Kiev oblast',
							'ЗАП' => 'Zaporozhia oblast',
							'ЛУ' => 'Luhansk oblast',
							'ДО' => 'Donetsk oblast',
							'ВІ' => 'Vinnitsa oblast',
							'АР' => 'Autonomous Republic of Crimea',
							'МК' => 'Mykolaiv oblast',
							'КІ' => 'Kirovohrad oblast',
							'СУ' => 'Sumy oblast',
							'ЛЬ' => 'Lviv oblast',
							'ЧЕ' => 'Cherkasy oblast',
							'ХМ' => 'Khmelnytskyi oblast',
							'ВО' => 'Volyn oblast',
							'РІ' => 'Rivne oblast',
							'ІВ' => 'Ivano-Frankivsk oblast',
							'ТЕ' => 'Ternopil oblast',
							'ЗАК' => 'Zakarpattia oblast',
							'ЧР' => 'Chernivtsi oblast'];
							asort($oblast);
							} elseif($lang == 'ru'){
							$oblast = [
							'ОД'=>'Одесская область ',
							'ДН'=>'Днепропетровская область',
							'ЧГ'=>'Черниговская область',
							'ХА'=>'Харьковская область',
							'ЖТ'=>'Житомирская область',
							'ПО'=>'Полтавская область ',
							'ХЕ'=>'Херсонская область',
							'КВ'=>'Киевская область',
							'ЗАП'=>'Запорожская область',
							'ЛУ'=>'Луганская область',
							'ДО'=>'Донецкая область',
							'ВІ'=>'Винницкая область',
							'АР'=>'Автономная Республика Крым',
							'МК'=>'Николаевская область',
							'КІ'=>'Кировоградская область',
							'СУ'=>'Сумская область',
							'ЛЬ'=>'Львовская область',
							'ЧЕ'=>'Черкасская область',
							'ХМ'=>'Хмельницкая область',
							'ВО'=>'Волынская область',
							'РІ'=>'Ровенская область',
							'ІВ'=>'Ивано-Франковская область',
							'ТЕ'=>'Тернопольская область',
							'ЗАК'=>'Закарпатская область',
							'ЧР'=>'Черновицкая область '];
							asort($oblast);
							}
							elseif($lang == 'ua-uk'){
							  $oblast = ['ОД' => 'Одеська область',
							'ДН' => 'Дніпропетровська область',
							'ЧГ' => 'Чернігівська область',
							'ХА' => 'Харківська область',
							'ЖТ' => 'Житомирська область',
							'ПО' => 'Полтавська область',
							'ХЕ' => 'Херсонська область',
							'КВ' => 'Київська область',
							'ЗАП' => 'Запорізька область',
							'ЛУ' => 'Луганська область',
							'ДО' => 'Донецька область',
							'ВІ' => 'Вінницька область',
							'АР' => 'Автономна Республіка Крим',
							'МК' => 'Миколаївська область',
							'КІ' => 'Кіровоградська область',
							'СУ' => 'Сумська область',
							'ЛЬ' => 'Львівська область',
							'ЧЕ' => 'Черкаська область',
							'ХМ' => 'Хмельницька область',
							'ВО' => 'Волинська область',
							'РІ' => 'Рівненська область',
							'ІВ' => 'Івано-Франківська область',
							'ТЕ' => 'Тернопільська область',
							'ЗАК' => 'Закарпатська область',
							'ЧР' => 'Чернівецька область'];
							asort($oblast);
							}
							?>
							<select class="form-control" name="oblast" <?php echo $stancom; ?>>
								<?php
									foreach($oblast as $key => $obs){ ?>
									<option value='<?= $key; ?>'><?= $obs; ?></option>
								<?php    }
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for=""><?php echo $entry_address_1; ?></label>
							<input type="text" class="form-control" name="address" <?php echo $stancom; ?>>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for=""><?php echo $entry_telephone; ?></label>
							<input type="tel" class="form-control" name="telephone" <?php echo $stancom; ?>>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for=""><?php echo $entry_email; ?> </label>
							<input type="email" class="form-control" name="email" <?php echo $stancom; ?>>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for=""><?php echo $type_oplatu; ?></label>
							<select name="payment_method" class="form-control" <?php echo $stancom; ?>>
								<?php if ($error_warning) { ?>
								<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
								<?php } ?>
								<?php if ($payment_methods) { ?>
								<?php $key =0; foreach ($payment_methods as $payment_method) { ?>
								<?php  if ($keyp == $key){   ?>
								<option value="<?php echo $payment_method['code']; ?>" selected="selected">VISA - MASTER CARD - PAYPAL<?php /*echo $payment_method['title'];*/ ?></option>
								<?php }$key++; ?>
								<?php } ?>
								<?php } ?>
							</select>
							<?php foreach ($payment_methods as $payment_method) { ?>
							<input type="hidden" name="<?php echo $payment_method['code']; ?>" value="<?php echo $payment_method['title']; ?>">
							<?php } ?>
						</div>
					</div>
				</div>
				<br />
				<div class="row checkbox-row">
					<div class="col-md-12 checkbox-header">
						<?php echo $osob; ?>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="health_problems" value="1" <?php echo $stancom; ?>>
							<label for="health_problems"><?php echo $bad1; ?></label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="poor_hearing"  value="2" <?php echo $stancom; ?>>
							<label for="poor_hearing"><?php echo $bad2; ?></label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="poor_sees" value="3" <?php echo $stancom; ?>>
							<label for="poor_sees"><?php echo $bad3; ?></label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="not_walking" value="4" <?php echo $stancom; ?>>
							<label for="not_walking"><?php echo $bad4; ?> </label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="radio" name="special_notes" id="other_defects" value="5" <?php echo $stancom; ?>>
							<label for="other_defects"><?php echo $bad5; ?></label>
						</div>
					</div>
				</div>
				<div class="row checkbox-row">
					<div class="col-md-12 checkbox-header">
						<?php echo $iak; ?>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="social_networks" value="1" <?php echo $stancom; ?>>
							<label for="social_networks"><?php echo $iak1; ?></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="search_sites" value="2" <?php echo $stancom; ?>>
							<label for="search_sites"><?php echo $iak2; ?></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="r_advertising" value="3" <?php echo $stancom; ?>>
							<label for="r_advertising"><?php echo $iak3; ?></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<input type="radio" name="referral" id="friends" value="4" <?php echo $stancom; ?>>
							<label for="friends"><?php echo $iak4; ?></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<br />
						<strong><?php echo $text_comments; ?></strong>
						<br />
						<br />
					</div>
					<div class="col-md-12">
						<textarea name="comment" rows="3" class="form-control" <?php echo $stancom; ?>><?php echo $comment; ?></textarea>
					</div>
				</div>

				<div class="shipping-method" style="display: none;">
					<?php if ($error_warning) { ?>
					<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
					<?php } ?>
					<?php if ($shipping_methods) { ?>
					<?php foreach ($shipping_methods as $shipping_method) { ?>
					<?php if (!$shipping_method['error']) { ?>
					<?php foreach ($shipping_method['quote'] as $quote) { ?>
					<div class="radio" >
						<label>
							<?php if ($quote['code'] == $code || !$code) { ?>
							<?php $code = $quote['code']; ?>
							<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"  title="<?php echo $quote['title']; ?>" checked="checked" />
							<?php } else { ?>
							<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"  title="<?php echo $quote['title']; ?>" checked="checked"/>
							<?php } ?>
							<?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></label>
					</div>
					<?php } ?>
					<?php } else { ?>
					<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
					<?php } ?>
					<?php } ?>
					<?php } ?>
				</div>

				<div class="your_order">

					<?php if ($text_agree) { ?>
					<div class="buttons clearfix">
						<div class="pull-right"><?php echo $text_agree; ?>
							<?php if ($agree) { ?>
							<input type="checkbox" name="agree" value="1" checked="checked" />
							<?php } else { ?>
							<input type="checkbox" name="agree" value="1" />
							<?php } ?>
							&nbsp;
						</div>
					</div>
					<?php } else { ?>
					<!--<div class="buttons">
					  <div class="pull-right">
						<input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="btn btn-primary" />
					  </div>
					</div>-->
					<?php } ?>
<?php if ($logged) { ?>
							<div class="payment clearfix" style="margin-top: 25px">
						<?php if ($payment) echo $payment; else { ?>
						<input type="button" class="btn btn-primary" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" id="button-register" value="<?php echo $heading_title;?>">
						<?php } ?>
					</div>	
							<?php }else{ ?>
							<div class="payment clearfix" style="margin-top: 25px">
						<input type="button" class="btn btn-primary" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" id="button-registered" value="<?php echo $button_reg;?>">
						
					</div>
							<?php } ?>
				
				</div>
				<h3><?php echo $text_cart;?></h3>
				<!--<table id="cart_table" class="table table-bordered table-hover table-responsive">
					<tfoot>
					<?php foreach ($totals as $total) { ?>
					<tr>
						<td colspan="4" class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
						<td class="text-right"><?php echo $total['text']; ?></td>
					</tr>
					<?php } ?>
					</tfoot>
				</table>-->
				<div style="display: none;">
					<input type="hidden" name="commission" value="<?=$commission; ?>">
					<input type="hidden" name="total_price_with_commission" value="<?= $totalPrice; ?>">
					<input type="hidden" name="total_price_with_commission_usd" value="<?= $curency_usd; ?>">
					<?php foreach($parcel as $item){ ?>
						<input type="hidden" name="parcel[<?=$item['count'];?>]" value="<?=$item['price'];?>">
					<?php } ?>
				</div>
				<div class="row trash-information ordering">
					<div class="col-sm-12 col-md-6">
						<div class="col-sm-12 padbot no-padding trash-information-row">
							<div class="col-xs-7 col-sm-5 no-padding">
								<?= $cart_number_of_products_text; ?>:
							</div>
							<div class="col-xs-5 col-sm-7 no-padding">
								<span id="cartCount"><?=$count; ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 padbot no-padding trash-information-row">
							<div class="col-xs-7 col-sm-5 no-padding">
								<?= $cart_weight_text; ?>:
							</div>
							<div class="col-xs-5 col-sm-7 no-padding">
								<span id="totalWeight"><?=$weight; ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 padbot no-padding trash-information-row">
							<div class="col-xs-7 col-sm-5 no-padding">
								<?= $parcel_count_text; ?>:
							</div>
							<div class="col-xs-5 col-sm-7 no-padding">
								<div id="parcel">
									<?php foreach($parcel as $item){ ?>
									<span><?=$item['count'];?> - <?=$item['price'];?> | </span>
									<?php } ?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="col-sm-12 padbot no-padding trash-information-row">
							<div class="col-xs-7 col-sm-5 no-padding">
								<?= $cart_cost_text; ?>:
							</div>
							<div class="col-xs-5 col-sm-7 no-padding">
								<span id="totalPrice"><?=$total['text']; ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 padbot no-padding trash-information-row">
							<div class="col-xs-7 col-sm-5 no-padding">
								<?= $cart_commission_text; ?>:
							</div>
							<div class="col-xs-5 col-sm-7 no-padding">
								<span id="commission"><?=$commission; ?> $</span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 padbot no-padding trash-information-row">
							<div class="col-xs-7 col-sm-5 no-padding">
								<?= $cart_the_total_cost_text; ?>:
							</div>
							<div class="col-xs-5 col-sm-7 no-padding">
							<?php  if ($keyp == 1){   ?>
								<span id="allTotalPrice">0.00 грн/0.00 $</span>
							<?php  }else{   ?>
							<span id="allTotalPrice"><?=$totalPrice; ?>/<?=$curency_usd;?></span>
							<?php  }   ?>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12">
						<div class="text-center">
							<!--<a href="<?php echo $checkout; ?>"
							   class="btn" style="width:100%;background-color: #ff0000 !important;border-radius:40px;box-shadow: 0 3px 5px rgba(0,0,0,0.2);border:0px;color: #ffffff !important;">
								<?php echo $button_checkout; ?>
							</a>-->
						</div>
						<div class="">
							<br />
							<a href="/index.php?route=checkout/cart"
							   class="btn to-cart">
								<?php echo $korzuna ?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
	</div>
</div>



<script type="text/javascript"><!--
	var error = true;

	// Login
	$(document).delegate('#button-login', 'click', function() {
		$.ajax({
			url: 'index.php?route=checkout/checkout/login_validate',
			type: 'post',
			data: $('.login-form :input'),
			dataType: 'json',
			beforeSend: function() {
				$('#button-login').button('loading');
			},
			complete: function() {
				$('#button-login').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					$('.login-form .message').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	// Register
	$(document).delegate('#button-register', 'click', function()
	{
		var payment_method = $(".checkout_form select[name='payment_method']").val();

		var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'tel\'], .checkout_form input[type=\'email\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
		data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery(".checkout_form input[name='"+payment_method+"']").val();

		$.ajax({
			url: 'index.php?route=checkout/checkout/validate',
			type: 'post',
			data: data,
			dataType: 'json',
			beforeSend: function() {
				$('#button-register').button('loading');
			},
			complete: function() {
				$('#button-register').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				} else if (json['error']) {
					error = true;
					if (json['error']['warning']) {
						$('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}

					for (i in json['error']) {
						$('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
					}
				} else
				{
					error = false;
					$('#button-register').button('loading');
					jQuery('select[name=\'payment_method\']').change();
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
    
    	// Registered
	$(document).delegate('#button-registered', 'click', function()
	{
	window.location.assign("/index.php?route=account/login");
	});

	$('select[name=\'country_id\']').on('change', function() {
		$.ajax({
			url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
			},
			complete: function() {
				$('.fa-spinner').remove();
			},
			success: function(json) {
				if (json['postcode_required'] == '1') {
					$('input[name=\'postcode\']').parent().parent().addClass('required');
				} else {
					$('input[name=\'postcode\']').parent().parent().removeClass('required');
				}

				html = '<option value=""><?php echo $text_select; ?></option>';

				if (json['zone'] && json['zone'] != '') {
					for (i = 0; i < json['zone'].length; i++) {
						html += '<option value="' + json['zone'][i]['zone_id'] + '"';

						if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
							html += ' selected="selected"';
						}

						html += '>' + json['zone'][i]['name'] + '</option>';
					}
				} else {
					html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
				}

				$('select[name=\'zone_id\']').html(html).val("");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});


	$('select[name=\'shipping_country_id\']').on('change', function() {
		$.ajax({
			url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
			},
			complete: function() {
				$('.fa-spinner').remove();
			},
			success: function(json) {
				if (json['postcode_required'] == '1') {
					$('input[name=\'postcode\']').parent().parent().addClass('required');
				} else {
					$('input[name=\'postcode\']').parent().parent().removeClass('required');
				}

				html = '<option value=""><?php echo $text_select; ?></option>';

				if (json['zone'] && json['zone'] != '') {
					for (i = 0; i < json['zone'].length; i++) {
						html += '<option value="' + json['zone'][i]['zone_id'] + '"';

						if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
							html += ' selected="selected"';
						}

						html += '>' + json['zone'][i]['name'] + '</option>';
					}
				} else {
					html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
				}

				$('select[name=\'shipping_zone_id\']').html(html).val("");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});





	$('select[name=\'country_id\'], select[name=\'zone_id\'], select[name=\'shipping_country_id\'], select[name=\'shipping_zone_id\'], input[type=\'radio\'][name=\'payment_address\'], input[type=\'radio\'][name=\'shipping_address\']').on('change', function()
	{
		if (this.name == 'contry_id') jQuery("select[name=\'zone_id\']").val("");
		if (this.name == 'shipping_country_id') jQuery("select[name=\'shipping_zone_id\']").val("");

		jQuery(".shipping-method").load('index.php?route=checkout/checkout/shipping_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function()
		{
			if (jQuery("input[name=\'shipping_method\']:first").length)
			{
				jQuery("input[name=\'shipping_method\']:first").attr('checked', 'checked').prop('checked', true).click();
			} else
			{
				jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
			}
		});

		jQuery(".payment-method").load('index.php?route=checkout/checkout/payment_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function()
		{
			jQuery("[name=\'payment_method\']").removeAttr("checked").prop('checked', false);
		});
	});


	$(document).delegate('input[name=\'shipping_method\']', 'click', function()
	{
		jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
	});

	$('body').delegate('[name=\'payment_method\']','change', function()
	{
		// $('#button-register').button('loading');
		var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
		data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form select[name=\'payment_method\']').val();

		if (!error)
			$.ajax({
				url: 'index.php?route=checkout/checkout/confirm',
				type: 'post',
				data: data,
				complete: function() {
					$('#button-register').button('reset');
				},
				success: function(html)
				{
					var href = jQuery(html).find('a').attr('href')
					if(typeof href != 'undefined'){
					  window.location.href = jQuery(html).find('a').attr('href');
					}else {
					  var tel = jQuery(html).find('#phone_payment')[0]
					  if(typeof tel != 'undefined'){
						jQuery(".payment").html(html);
					  }else{
						html = jQuery(html).hide()
						jQuery(".payment").append(html);
						jQuery("#button-confirm").click();
					  }
					}

				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
	});

	$('select[name=\'country_id\']').trigger('change');
	jQuery(document).ready(function()
	{
        <?php if (!$logged) { ?>
                           var add = document.getElementById('add').title;
				if (add == 'Українська') {
					lab1 = 'Ввійти або зареєструватися';
                    lab2 = 'Закрити';
                    messagepok = 'Покупка доступна лише авторизованим користувачам';
				}
				if (add == 'russian') {
					lab1 = 'Войти или зарегистрироваться';
                    lab2 = 'Закрыть';
                    messagepok = 'Покупка доступна только авторизованным пользователям';
				}
				if (add == 'English') {
					lab1 = 'Login or register';
                    lab2 = 'Close';
                    messagepok = 'Purchase is available only to authorized users';
				}
       	bootbox.dialog({
		message: messagepok,
			buttons: {
			success: {
		    	label: lab1,
				className: "btn-to-cart showbutton",
					callback: function() {
						window.location.assign("/index.php?route=account/login");
										}
										},
					main: {
						label: lab2,
                        className: "btn-to-shop",
					}
						}
													});
                         <?php } ?>
     
        
		jQuery('input:radio[name=\'payment_method\']:first').attr('checked', true).prop('checked', true);
		<?php /*if ($opencart_version < 2000) {?>
			$('.colorbox').colorbox({
			width: 640,
			height: 480
			});
	 <?php }*/?>
	});
	//--></script>

<?php echo $footer;?>
