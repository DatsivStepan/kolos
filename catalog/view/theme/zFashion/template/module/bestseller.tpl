</div><h3 class="text-center">РЕКОМЕНДОВАНІ ТОВАРИ</h3>
  <div class="product-content">
  <div class="row">
  <?php foreach ($products as $product) { ?>
      <div class="product-id-<?=$product['product_id'];?> fiveparts">
          <div class="product-thumb transition" style="box-shadow: 0px 1px 8px 0px rgba(136, 136, 136, 0.37);padding-bottom:15px;padding-top:15px;border-radius: 8px">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <div class="image">
                  <a href="<?php echo $product['href']; ?>">
                      <img src="<?php echo $product['thumb']; ?>"
                           alt="<?php echo $product['name']; ?>"
                           title="<?php echo $product['name']; ?>"
                           class="img-responsive"/>
                  </a>
              </div>
              <div class="caption">
                  <div class="row no-padding">
                      <div class="col-sm-6 col-xs-6">
                          <?php if ($product['price']) { ?>
                          <p class="price" style="float: left; padding-right: 3%; font-size: 90%;">
                              <?php if (!$product['special']) { ?>
                              <?php echo $product['price']; ?>
                              <?php } else { ?>
                              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                              <?php } ?>
                          </p>
                          <?php } ?>
                      </div>
                      <div class="col-sm-6 col-xs-6">
                          <i class="fa fa-minus minusQuantityCount" style="color:#adadae;cursor:pointer;"></i>
                          <input type="text" name="quantity" value="<?=$product['minimum']?>" size="1"  class="text-center input-quantity"  style="border-radius: 7px 7px 7px 7px;"/>
                          <i class="fa fa-plus plusQuantityCount" style="color:#adadae;cursor:pointer;"></i>
                      </div>
                  </div>
                  <div class="row no-padding">
                      <div class="col-sm-4 no-padding purse col-xs-4"></div>
                      <div class="col-sm-8 col-xs-8">
                          <button type="button" class="btn button_cart" onclick="cart.add('<?php echo $product['product_id']; ?>', $('.product-id-<?=$product['product_id'];?>').find('.input-quantity').val())"  style="width:100%;margin-top:7px;box-shadow: 0px 3px 3px 0px rgba(136, 136, 136, 0.57);"> <?php echo $button_cart; ?></button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  <?php } ?>
  
  
  <?php foreach ($products as $product) { ?>
  <div class="product-layout-col">
   <div class="product-thumb transition">
   	<div class="col-sm-4">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
    </div>
       <div class="col-sm-8">
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
     </div>
   </div>
  </div>
  <?php } ?>
  </div>
  </div>
