<div class="sticky-menu">
    <div class="menured"><?= $heading_title; ?></div>
        <ul class="customdrop">
            <?php $c=0; foreach ($categories as $category) { ?>
                <?php
                    $classes = '';
                    $display = 'display:none;';
                    $icon = ' fa-angle-down ';
                    $c++;
                    if ($category['category_id'] == $category_id) {
                        $classes .= 'active';
                        $display = '';
                        $icon = ' fa-angle-up ';
                    }
                ?>
            <?php if($c<=11) { ?>
                <li class='<?= $classes; ?>'>
                            <?php /* ?>
                                <a href="<?php echo $category['href'];?>" class='clickParentMenu'  title="<?php echo $category['name'];?>">
                            <?php */ ?>
                    <a class="clickParentMenu"  title="<?php echo $category['name'];?>">
                        <?php if($category['thumb']){ ?>
                            <img src="<?= $category['thumb']; ?>" style="width:18px;margin-right:10px;">
                        <?php } ?>
                        <span data-href="<?= $category['href']; ?>" style="cursor:pointer;" class="clickCategoryName">
                            <?php echo $category['name']; ?>
                        </span>
                        <i class="arrow-icon fa <?= $icon; ?> pull-right" style='cursor:pointer;font-size:18px;'></i>
                    </a>
                    <?php if($category['children']) { ?>
                        <ul class="dropDownSubMenu" style='<?= $display; ?>'>
                                <?php
                                    foreach ($category['children'] as $child)
                                    {
                                ?>
                                <li>
                                    <a href="<?php echo $child['href'];?>" tabindex="-1" title="<?php echo $child['name'];?>"><?php echo $child['name'];?></a>
                                        </li>
                                <?php } ?>
                        </ul>
                    <?php }  ?>
                </li>
            <?php } ?>
            <?php } ?>
        </ul>
    <ul id="more" class="customdrop" style="    display: none;">
        <?php $c=0; foreach ($categories as $category) { ?>
        <?php
                    $classes = '';
                    $display = 'display:none;';
                    $icon = ' fa-angle-down ';
                    $c++;
                    if ($category['category_id'] == $category_id) {
                        $classes .= 'active';
                        $display = '';
                        $icon = ' fa-angle-up ';
                    }
                ?>
        <?php if($c>11) { ?>
        <li class='<?= $classes; ?>'>
            <?php /* ?>
            <a href="<?php echo $category['href'];?>" class='clickParentMenu'  title="<?php echo $category['name'];?>">
                <?php */ ?>
                <a class='clickParentMenu'  title="<?php echo $category['name'];?>">
                    <?php if($category['thumb']){ ?>
                    <img src="<?= $category['thumb']; ?>" style="width:18px;margin-right:10px;">
                    <?php } ?>
                        <span data-href="<?= $category['href']; ?>" style="cursor:pointer;" class="clickCategoryName">
                            <?php echo $category['name']; ?>
                        </span>
                    <i class='fa <?= $icon; ?> pull-right' style='cursor:pointer;font-size:18px;'></i>
                </a>
                <?php if($category['children']) { ?>
                <ul class="dropDownSubMenu" style='<?= $display; ?>'>
                    <?php
                                    foreach ($category['children'] as $child)
                                    {
                                ?>
                    <li>
                        <a href="<?php echo $child['href'];?>" tabindex="-1" title="<?php echo $child['name'];?>"><?php echo $child['name'];?></a>
                    </li>
                    <?php } ?>
                </ul>
                <?php }  ?>
        </li>
        <?php } ?>
        <?php } ?>
    </ul>
    <div id="namemore" class="menured text-center" onclick="my_f('more')"      style="text-indent: 0em; display: block;"><i class="fa fablack fa-arrow-down" aria-hidden="true"></i></div>
    <div class="mobile-sticky">
        TEST
    </div>

</div>
<script>
    function my_f(objName) {
        var morename = '<i class="fa fablack fa-arrow-up" aria-hidden="true"></i>';
        var minname = '<i class="fa fablack fa-arrow-down" aria-hidden="true"></i>';
        var namemore = document.getElementById(namemore);
        //document.getElementById('namemore').innerHTML = name;
        document.getElementById('namemore').innerHTML == morename ? document.getElementById('namemore').innerHTML = minname : document.getElementById('namemore').innerHTML = morename
        var object = document.getElementById(objName);
        object.style.display == 'none' ? object.style.display = 'block' : object.style.display = 'none'

    }
</script>