<div class="row">
	<div class="col-sm-3 hidden-xs">
		<img src="<?= $kozakrec; ?>" alt="">
	</div>
	<div class="col-sm-9 text-center">
		<h3 class="featured-products-header text-center">
			<?php echo $heading_title; ?>
		</h3>
	</div>
</div>

<div class="product-content">
	<div class="row">
		<?php foreach ($products as $product) { ?>
			<div class="col-sm-6 col-md-4 col-lg-3 product-item product-id-<?=$product['product_id'];?>">
				<?php if ($product['special']) { ?>
					<span class="ribbon">HOT<b></b></span>
				<?php } ?>
				<div class="product-thumb transition">
					<div class=" nd-wrap nd-style-1">
						<h4>
							<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
						</h4>
						<center>
							<a href="<?php echo $product['href']; ?>">
								<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
							</a>
						</center>
					</div>
					<div class="caption">
						<div class="row no-margin text-center">
							<div class="col-xs-12">
								<?php if ($product['price']) { ?>
									<p class="price">
										<?php if (!$product['special']) { ?>
										<?php echo $product['price']; ?>
										<?php } else { ?>
										<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
											<?php } ?>
									</p>
								<?php } ?>
							</div>
							<div class="col-xs-12">
								<i class="charges fa fa-minus minusQuantityCount"></i>
								<input type="text" name="quantity" value="<?=$product['minimum']?>" size="1"  class="text-center input-quantity" />
								<i class="charges fa fa-plus plusQuantityCount"></i>
							</div>
						</div>
						<div class="row add-block">
							<div class="purse">
							</div>
							<button type="button" class="btn button_cart" onclick="cart.add('<?php echo $product['product_id']; ?>', $('.product-id-<?=$product['product_id'];?>').find('.input-quantity').val());"> <?php echo $button_cart; ?></button>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<?php foreach ($products as $product) { ?>
			<div class="product-layout-col">
				<div class="product-thumb transition">
					<div class="col-sm-4">
						<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="caption">
							<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
							<?php if ($product['rating']) { ?>
								<div class="rating">
									<?php for ($i = 1; $i <= 5; $i++) { ?>
										<?php if ($product['rating'] < $i) { ?>
											<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
										<?php } else { ?>
											<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
										<?php } ?>
									<?php } ?>
								</div>
							<?php } ?>
							<?php if ($product['price']) { ?>
								<p class="price">
									<?php if (!$product['special']) { ?>
										<?php echo $product['price']; ?>
									<?php } else { ?>
										<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
									<?php } ?>
								</p>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
