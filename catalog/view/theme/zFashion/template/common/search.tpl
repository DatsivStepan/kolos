<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
  <span class="input-group-btn" style="
    z-index: 99;
"><button type="button" class="btn btn-default btn-lg">
    <i class="fa fa-search" style="
    display: inline-block;
    float: none;
    vertical-align: middle;
    
"></i></button>
  </span>
</div>