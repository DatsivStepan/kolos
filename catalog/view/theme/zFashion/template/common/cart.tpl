
 <div class="bottom-line">
   <div style="display: none">
   <audio id="beep-one" controls="controls" preload="auto" style="    position: fixed; visibility: hidden;">
     <source src="/image/yvd.mp3"></source>
   </audio>
   </div>
      <div class="col-xs-4 no-padding text-right">
          <?php if($text_items > 0){ ?>
            <img src="../../image/mishok_cart.png" class="CartImageHeader" alt="">
          <?php }else{ ?>
            <img src="../../image/mishok.png" class="CartImageHeader" alt="">
          <?php } ?>
      </div>
     <div class="col-xs-8 no-padding">
        <div id="cart" >
         <span  data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="dropdown-toggle">
          <span class="product-count"><?php echo $cartt; ?></span>
          <br>
          <span id="cartt">(<?php echo $text_items; ?>)</span><?php echo $cart_items; ?>
          <img src="image/trik.png" alt=""></span>
         <ul class="dropdown-menu cart-dropdown pull-right">
           <?php if ($products || $vouchers) { ?>
           <li>
             <table class="table table-striped">
               <?php foreach ($products as $product) { ?>
               <tr>
                 <td class="text-center"><?php if ($product['thumb']) { ?>
                   <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                   <?php } ?></td>
                 <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                   <input type="hidden" class="added_product_id" value="<?=$product['product_id']?>">
                   <?php if ($product['option']) { ?>
                   <?php foreach ($product['option'] as $option) { ?>
                   <br />
                   - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                   <?php } ?>
                   <?php } ?>
                   <?php if ($product['recurring']) { ?>
                   <br />
                   - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                   <?php } ?></td>
                 <td class="text-right">x <?php echo $product['quantity']; ?></td>
                 <td class="text-right"><?php echo $product['total']; ?></td>
                 <td class="text-center"><button type="button" onclick="cart.remove('<?php echo $product['key']; ?>', <?=$product['product_id']?>);" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
               </tr>
               <?php } ?>
               <?php foreach ($vouchers as $voucher) { ?>
               <tr>
                 <td class="text-center"></td>
                 <td class="text-left"><?php echo $voucher['description']; ?></td>
                 <td class="text-right">x&nbsp;1</td>
                 <td class="text-right"><?php echo $voucher['amount']; ?></td>
                 <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
               </tr>
               <?php } ?>
             </table>
           </li>
           <li>
             <div>
               <table class="table table-bordered">
                 <?php foreach ($totals as $key => $total) { ?>
                 <?php if($key == 0){ ?>
                     <tr style='display:none;'>
                 <?php }else{ ?>
                     <tr>
                 <?php } ?>
                   <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
                   <td class="text-right"><?php echo $total['text']; ?></td>
                 </tr>
                 <?php } ?>
               </table>
               <p class="text-right">
               <a href="" onclick="$('#cart').load('index.php?route=checkout/cart&remove_all' + ' #cart > *'); location.reload();  return false; "><strong><i class="fa fa-eraser"></i><?php echo $button_clear; ?></strong></a>&nbsp;&nbsp;&nbsp;
               <a href="<?php echo $cart; ?>" ><strong><i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?></strong></a>&nbsp;&nbsp;&nbsp;
               <a href="<?php echo $checkout; ?>" ><strong><i class="fa fa-share"></i> <?php echo $text_checkout; ?></strong></a></p>
             </div>
           </li>
           <?php } else { ?>
           <li>
             <p class="text-center"><?php echo $text_empty; ?></p>
           </li>
           <?php } ?>
         </ul>
       </div>
    </div>
    <div class="clearfix"></div>
</div>

