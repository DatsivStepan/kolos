</div>
</div>
</div>
  <div class="footer-top-line text-center">
    <hr>
    <img src="image/kol-foo.png" alt=""/>
  </div>
  <div class="footer">
    <footer class="container">
    <div class="row">
      <div class="col-sm-3 col-xs-12">

        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                              alt="<?php echo $name; ?>" class="img-responsive"/></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>

      <div class="col-sm-5 col-xs-12 text-center no-padding">
        <div class="menu-footer">
          <?php if ($informations) { ?>
          <?php foreach ($informations as $key => $information) { ?>
          <?php if(($key !== 1) && ($key !== 2) && ($key !== 3) ){ ?>
          <a href="<?php echo $information['href']; ?><?php echo $information['id']; ?>"><?php echo $information['title']; ?> </a>
          <?php } ?>
          <?php if($key == 1){ ?>
          <a href="/index.php?route=information/akcii"><?php echo $information['title']; ?> </a>
          <?php } ?>
          <?php if($key == 2){ ?>
          <a href="/index.php?route=affiliate/login"><?php echo $information['title']; ?> </a>
          <?php } ?>
          <?php if($key == 3){ ?>
          <a href="/index.php?route=information/zauvajennia"><?php echo $information['title']; ?> </a>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </div>
      </div>

      <div class="col-sm-3 col-xs-12 text-right footer-button">
        <div class="row">
          <div class="col-sm-6 col-xs-6 no-padding text-center payment-icon">
            <div class="right-border"></div>
            <a target="_blank" href="https://www.paypal.com"><img src="../image/paypal.png" alt=""/></a>
            <a target="_blank" href="http://www.visa.com.ua/"><img src="../image/visa.png" alt=""/></a>
            <a target="_blank" href="http://www.mastercard.com/"><img src="../image/mastercard.png" alt=""/></a>
          </div>
          <div class="col-sm-6 col-xs-6 no-padding text-center">
            <a target="_blank" href="https://vk.com/kolosexpress"><img src="../image/vk.png" alt=""/></a>
            <a target="_blank" href="https://twitter.com/kolosexpress"><img src="../image/tv.png" alt=""/></a>
            <a target="_blank" href="https://facebook.com/kolosexpress"><img src="../image/fa.png" alt=""/></a>
          </div>


        </div>
        <div style="clear:both;">
        </div>
      </div>
    </div>
    </footer>

  </div>
</body>
</html>