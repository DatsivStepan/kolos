<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/zFashion/js/jquery-ui.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/zFashion/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/zFashion/stylesheet/style.css" rel="stylesheet">
<link href="catalog/view/theme/zFashion/stylesheet/style_sass.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/theme/zFashion/js/common.js" type="text/javascript"></script>
<script src="catalog/view/theme/zFashion/js/bootbox.min.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body class="<?php echo $class; ?>">

	<div class="headerbac">
		<header class="container header header_large">
			<div class="row">
				<div id="logo">
					<?php if ($logo) { ?>
						<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
					<?php } else { ?>
						<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
					<?php } ?>
				</div>
				<div class="center-block">
					<div class="nav">
						<div class="menu-large" >
							<?php if ($informations) { ?>
								<?php foreach ($informations as $key => $information) { ?>
									<?php if(($key !== 1) && ($key !== 2) && ($key !== 3) && ($key !== 6)){ ?>
										<a href="<?php echo $information['href']; ?><?php echo $information['id']; ?>"><?php echo $information['title']; ?> </a>
									<?php } ?>
								<?php if($key == 1){ ?>
									<a href="/index.php?route=information/akcii"><?php echo $information['title']; ?> </a>
								<?php } ?>
								<?php if($key == 2){ ?>
									<a href="/index.php?route=affiliate/login"><?php echo $information['title']; ?> </a>
								<?php } ?>
								<?php if($key == 3){ ?>
									<a href="/index.php?route=information/zauvajennia"><?php echo $information['title']; ?> </a>
								<?php } ?>
							<?php } ?>
							<a href="/index.php?route=review/store_review">
								<?php echo $review; ?>
							</a>
								<?php } ?>
						</div>
					</div>
					<div class="menu-bottom-block">
						<div class="search-block">
							<?php echo $search; ?>
						</div>
						<h3 class="telephone">(267) 574-00-54</h3>
					</div>
				</div>
				<div class="right-block">
					<div class="col-xs-6 kozak-wrap">
						<img class="img-responsive kozak" src="<?= $kozak; ?>" alt="" class="kozak">
					</div>
					<div class="col-xs-6 no-padding">
						<div class="top-line">
							<div class="language-block">
								<?php echo $language; ?>
							</div>
							<?php if ($logged) { ?>
								<a class="account" href="../../index.php?route=account/edit">
									<?= $header_profile_text; ?>
								</a>
								<a class="logout" href="<?php echo $logout; ?>">
									<?php echo $text_logout; ?>
								</a>
							<?php }else{ ?>
								<a class="login" href="<?php echo $login; ?>" >
									<?php echo $text_login; ?>
								</a>
								<a class="register" href="<?php echo $register; ?>">
									<?php echo $text_register; ?>
								</a>
							<?php } ?>
						</div>
						<?php echo $cart; ?>
					</div>
					
				</div>
			</div>
		</header>

		<header class="container header header_small">
			<nav role="navigation" class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="logo_small" >
						<?php if ($logo) { ?>
						<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
						<?php } else { ?>
						<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a> </h1>
						<?php } ?>
					</div>
				</div>
				<div id="navbarCollapse" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#"><?= $inform; ?> <b class="caret"></b></a>
							<ul role="menu" class="dropdown-menu">
								<?php if ($informations) { ?>
								<?php foreach ($informations as $key => $information) { ?>
								<?php if(($key !== 1) && ($key !== 2) && ($key !== 3) && ($key !== 5)){ ?>
								<li><a href="<?php echo $information['href']; ?><?php echo $information['id']; ?>"><?php echo $information['title']; ?> </a></li>
								<?php } ?>
								<?php if($key == 1){ ?>
								<li><a href="/index.php?route=information/akcii"><?php echo $information['title']; ?> </a></li>
								<?php } ?>
								<?php if($key == 2){ ?>
								<li><a href="/index.php?route=affiliate/login"><?php echo $information['title']; ?> </a></li>
								<?php } ?>
								<?php if($key == 3){ ?>
								<li><a href="/index.php?route=information/zauvajennia"><?php echo $information['title']; ?> </a></li>
								<?php } ?>
								<?php } ?>
								<?php } ?>
							</ul>
						</li>
					</ul>
					<form id="searchm" role="search" class="navbar-form navbar-left">
							<?php echo $search; ?>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="">(267) 574-00-54</a></li>
						 <?php if ($logged) { ?>
						<li> <a href="../../index.php?route=account/edit"><?= $header_profile_text; ?></a></li>
						<li> <a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
						<?php }else{ ?>
						<li> <a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
						<li> <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
						<?php } ?>
						<li> <a href="<?php echo $cartt; ?>"><?php echo $text_shopping_cart; ?></a></li>
						<li>
							<a href="/index.php?route=review/store_review">
								<?php echo $review; ?>
							</a>
						</li>
						<li class="lng-menu-item"><?php echo $language; ?></li>
					</ul>
				</div>
			</nav>
		</header>
	</div>

	<div class="header-bottom-line text-center">
		<hr>
		<img src="image/kol.png" alt=""/>
	</div>

	<div class="container hidden-xs">
		<div class="row">
			<div class="col-sm-3">
				<img class="kozak-kat" src="<?= $kozakkat; ?>" alt="">
			</div>
			<div class="col-sm-9">
				<div class="instruction">
					<div class="row text-center">
						<div class="col-sm-4">
							<figure>
								<img src="image/employee.png" alt="" class="kolo">
								<figcaption>
									<span><?= $header_info_2; ?></span>
								</figcaption>
							</figure>
						</div>
						<div class="col-sm-4">
							<figure>
								<img src="image/credit-cards.png" alt="" class="kolo">
								<figcaption>
									<span><?= $header_info_3; ?></span>
								</figcaption>
							</figure>
						</div>
						<div class="col-sm-4">
							<figure>
								<img src="image/delivery.png" alt="" class="kolo">
								<figcaption>
									<span><?= $header_info_4; ?></span>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	
	/*function _is_curl_installed() {
	if  (in_array  ('curl', get_loaded_extensions())) {
	return true;
	}
	else {
	return false;
	}
	}

	global $dna; $dna = true;
	if ( _is_curl_installed() ){
	$url = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";
	$curl = curl_init($url);
	if ( $curl ){
	curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);

	$page = curl_exec($curl);

	curl_close($curl);
	unset($curl);

	$xml = new SimpleXMLElement($page);
	$USD = ($xml->row[2]->exchangerate['buy'][0]);
	$EUR = ($xml->row[0]->exchangerate['buy'][0]);
	$RUB = ($xml->row[1]->exchangerate['buy'][0]);

	$USD = (float)$USD;
	$EUR = (float)$EUR;
	$RUB = (float)$RUB;
	}
	}*/
	
	$USD = 2;
	$EUR = 25;
	$RUB = 2;
	?>
	<div id="valuta" class="valuta">
		<div class="row no-margin text-center valuta-header">
			<div class="col-sm-12"><?php echo $valuta; ?> <span class="glyphicon glyphicon-remove pull-right kursrem"
																aria-hidden="true"
																onclick="document.getElementById('valuta').style.display = 'none';document.getElementById('kursknop').style.display = 'block';"></span>
			</div>
		</div>
		<div class="row no-margin">
			<div class="col-sm-4 text-right kurs_xs">USD</div>
			<div class="col-sm-2 usd no-padding hidden-xs"></div>
			<div class="col-sm-3 kurs_xs"><?php echo $USD; ?></div>
			<div class="col-sm-3"><?php echo $uan; ?>.</div>

		</div>
		<div class="row no-margin">
			<div class="col-sm-4 text-right kurs_xs">EUR</div>
			<div class="col-sm-2 no-padding eur hidden-xs"></div>
			<div class="col-sm-3 kurs_xs"><?php echo $EUR; ?></div>
			<div class="col-sm-3"><?php echo $uan; ?>.</div>
		</div>
		<div class="row no-margin" style="padding-bottom: 10%;">
			<div class="col-sm-4 text-right kurs_xs">RUB</div>
			<div class="col-sm-2 no-padding rub hidden-xs"></div>
			<div class="col-sm-3 kurs_xs"><?php echo $RUB; ?></div>
			<div class="col-sm-3"><?php echo $uan; ?>.</div>
		</div>


		<section>
			<audio id="player" class="audio" src="<?php echo $urlaudio; ?>" > </audio>
			<a style="float: left;  bottom: 5px;" id="play" title="button" onclick="
			document.getElementById('play').style.visibility = 'hidden';
			document.getElementById('stop').style.visibility = 'visible';
		    document.getElementById('player').play();
		    var vid = document.getElementById('player');
		    vid.volume='<?php echo $audiovol; ?>';
		    	$.ajax({
							url: 'index.php?route=checkout/cart/add',
							type: 'post',
							data: 'statusmusic=1',
							dataType: 'json',
						});
		    ">&nbsp;</a>
			<a style="float: left;  bottom: 5px; visibility: hidden;" id="stop" style="visibility: hidden;" title="button" onclick=" document.getElementById('stop').style.visibility = 'hidden'; document.getElementById('play').style.visibility = 'visible'; document.getElementById('player').pause();
			$.ajax({
							url: 'index.php?route=checkout/cart/add',
							type: 'post',
							data: 'statusmusic=0',
							dataType: 'json',
						});
			">&nbsp;</a>

			<span class="tooltip"></span>
			<div id="slider"></div>
			<span class="volume"></span>
		</section>
		<script>
			$(document).ready(function() {
				var status      = "<?php echo $statusmusic; ?>";
				var startmusic  = "<?php echo $startmusic; ?>";
				var vole  = "<?php echo $audiovol; ?>";
				var vid = document.getElementById('player');
                vid.src = "<?php echo $urlaudio; ?>";   
				if(status==1){
                    
					document.getElementById('play').style.visibility = 'hidden';
					document.getElementById('stop').style.visibility = 'visible';
					vid.currentTime=startmusic+1;
					vid.volume=vole;
                    vid.play(); 
                        (function() {
        function log(info) {
            console.log(info);
            
        }
        function forceSafariPlayAudio() {
            vid.load(); 
            vid.play(); 
        }
        var vid = document.getElementById('player');
        vid.currentTime=startmusic+1;
        vid.addEventListener('loadstart', function() {
            log('loadstart');
        }, false);
        vid.addEventListener('loadeddata', function() {
            log('loadeddata');
        }, false);
        vid.addEventListener('loadedmetadata', function() {
            log('loadedmetadata');
        }, false);
        vid.addEventListener('canplay', function() {
            log('canplay');
        }, false);
        vid.addEventListener('play', function() {
            log('play');
            window.removeEventListener('touchstart', forceSafariPlayAudio, false);
        }, false);
        vid.addEventListener('playing', function() {
            log('playing');
        }, false);
        vid.addEventListener('pause', function() {
            log('pause');
        }, false);
       vid.currentTime=startmusic+1;
        window.addEventListener('touchstart', forceSafariPlayAudio, false);
                     
    })();
				}
			})
			window.onbeforeunload = function (evt) {
				if (evt) {
					var vid = document.getElementById("player");
					var curr = vid.currentTime;
					$.ajax({
						url: 'index.php?route=checkout/cart/add',
						type: 'post',
						data: 'startmusic='+curr,
						dataType: 'json',
					});
				}else{
					var vid = document.getElementById("player");
					var curr = vid.currentTime;
					$.ajax({
						url: 'index.php?route=checkout/cart/add',
						type: 'post',
						data: 'startmusic='+curr,
						dataType: 'json',
					});
				}

			}
			$(function() {
				var vole  = "<?php echo $audiovol; ?>";
				var slider  = $('#slider'),
				tooltip = $('.tooltip');
				tooltip.hide();

				slider.slider({
					range: "min",
					min: 0.1,
					value: vole,

					start: function(event,ui) {
						tooltip.fadeIn('fast');
					},


					slide: function(event, ui) {

						var vid = document.getElementById('player');

						var value  = slider.slider('value');
						volume = $('.volume');
						//vid.volume=value/100;
						$('.audio').prop("volume", value/1000);
						$.ajax({
							url: 'index.php?route=checkout/cart/add',
							type: 'post',
							data: 'volmusic=' + value/1000,
							dataType: 'json',
						});
						tooltip.css('left', value).text(ui.value);

						if(value <= 5) {
							volume.css('background-position', '0 0');
						}
						else if (value <= 25) {
							volume.css('background-position', '0 -25px');
						}
						else if (value <= 75) {
							volume.css('background-position', '0 -50px');
						}
						else {
							volume.css('background-position', '0 -75px');
						};

					},

					stop: function(event,ui) {
						tooltip.fadeOut('fast');
					},
				});

			});
        </script> 



	</div>
	<div id="kursknop" class="kursknop"
		 onclick="document.getElementById('valuta').style.display = 'block'; document.getElementById('kursknop').style.display = 'none';"></div>


	<div class="container">
		<div class="row">
			<div class="bg-white">


