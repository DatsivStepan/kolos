<?php echo $header; ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-8 col-md-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <ul class="breadcrumb">
                <li><a href="<?php echo $base; ?>index.php?route=common/home"><i class="fa fa-home"></i></a></li>
                <li><a href="<?php echo $base; ?>index.php?route=information/zauvajennia"><?php echo $zauv; ?></a></li>
            </ul>
            <h3 class="remark-header"><?php echo $zauv; ?></h3>

            <?php if ($stan==1) { ?>
            <div class="row text-center"><span
                        class="label label-primary"><?php echo $corect; ?></span></div>
            <?php } ?>
            <?php if ($stan==0) { ?>
            <div class="row text-center"><span class="label label-danger"><?php echo $erore; ?></span>
            </div>
            <?php } ?>
            <div class="row" style="    padding-bottom: 25%;">

                <form role="form" action="<?php echo $base; ?>index.php?route=information/zauvajennia" method="post">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">

                        <div class="form-group">
                            <label for="InputName"><?php echo $name; ?></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="InputName" id="InputName" required>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
                        </div>

                        <div class="form-group">
                            <label for="InputEmail"><?php echo $mail; ?></label>
                            <div class="input-group">
                                <input type="email" class="form-control" id="InputEmail" name="InputEmail" required>
                                <span class="input-group-addon"><i
                                            class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
                        </div>

                        <div class="form-group">
                            <label for="InputReal"><?php echo $tema; ?></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="InputReal" id="InputReal" required>
                                <span class="input-group-addon"><i
                                            class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
                        </div>

                        <div class="form-group">
                            <label for="InputMessage"><?php echo $text; ?></label>
                            <div class="input-group"
                            >
                                <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required></textarea>
                                <span class="input-group-addon"><i
                                            class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
                        </div>

                        <input type="submit" name="submit" id="submit" value="<?php echo $send; ?>" class="btn remark-action pull-right">
                    </div>
                    <div class="col-lg-2"></div>
                </form>

            </div>

        </div>

    </div>


</div>

<?php echo $footer; ?>