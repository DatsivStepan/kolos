<?php echo $header; ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-8 col-md-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <ul class="breadcrumb">
                <li><a href="<?php echo $base; ?>index.php?route=common/home"><i class="fa fa-home"></i></a></li>
                <li><a href="<?php echo $base; ?>index.php?route=information/akcii" style="text-transform:uppercase;"><?php echo $acii; ?></a></li>
            </ul>
            <h3 class="action-header" style="text-transform:uppercase;"><?php echo $acii; ?>:</h3>


            <?php foreach ($banners as $banner) { ?>
            <div class="border_page rewiew">

                <?php if ($banner['link']) { ?>
                <h4 class="text-center"><a href="<?php echo $banner['link']; ?>"><?php echo $banner['title']; ?></a><
                </h4>
                <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>"
                                                              alt="<?php echo $banner['title']; ?>"
                                                              class="img-responsive"/></a>
                <?php } else { ?>
                <h4 class="text-center"><?php echo $banner['title']; ?></h4>
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"
                     class="img-responsive"/>
                <?php } ?>
            </div>
            <?php } ?>
            <div class="banner_pad"></div>

        </div>


    </div>
</div>
<?php echo $footer; ?>