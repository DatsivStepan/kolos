<?php echo $header; ?>
<div class="container">
  <!--<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <!-- <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="prof">
        <ul class="nav nav-tabs">
            <li role="presentation" >
                <a href="../../../index.php?route=account/edit"><?php echo $heading_title1; ?></a>
            </li>
            <li role="presentation">
                <a  href="../../../index.php?route=account/order"><?php echo $heading_zam; ?></a>
            </li>
            <li role="presentation">
                <a href="/index.php?route=account/notification"><?php echo $heading_spov; ?>
                  <?php if($notifications_count) {?>
                  <div class="notif">
                    <?=$notifications_count?>
                  </div>
                  <?php } ?>
                </a>
            </li>
            <li role="presentation" class="active">
                <a href="/index.php?route=account/coupon"><?php echo $heading_kyp; ?></a>
            </li>
        </ul>
      <h3 class="text-center" style=" text-transform: uppercase;">

          <?php echo $heading_kyp; ?>

      </h3>
      <div class="coupon-row">
        <?php foreach($coupons as $coupon) {?>
          <?php
          $today = getdate();
          $date_end = strtotime($coupon['date_end']);
          $date_start = strtotime($coupon['date_start']);
          ?>

        <?php if(($date_start < $today[0] )&&($date_end > $today[0] )){ ?>
            <div class="col-xs-12">
        <?php  }else{ ?>
            <div class="col-xs-12" style="color: rgba(125, 125, 125, 0.5);">
        <?php  } ?>

          <div class="col-xs-4">
            <?=$coupon['name']?>
          </div>
          <div class="col-xs-4">
            <?=$coupon['code']?>
          </div>
          <div class="col-xs-4">
            <?php if($coupon['shipping']) { ?>
            <p>Безкоштвна доставка</p>
            <?php } ?>
            <?php if($coupon['discount']>0) { ?>
            <p>
              <?=$coupon['discount']?>
              <?php if($coupon['type']=='P') { ?>
                &#37
              <?php } else{ ?>
                грн.
              <?php } ?>
            </p>
            <?php } ?>
          </div>
        </div>
        <hr>
        <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?>
    <div class="clearfix"></div>
    </div>
</div>
</div>
</div>
<?php echo $footer; ?>
