<?php echo $header; ?>
<div class="container">
  <!--<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <!-- <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="prof">
        <ul class="nav nav-tabs">
            <li role="presentation" >
                <a href="../../../index.php?route=account/edit"><?php echo $heading_title1; ?></a>
            </li>
            <li role="presentation">
                <a  href="../../../index.php?route=account/order"><?php echo $heading_zam; ?></a>
            </li>
            <li role="presentation" class="active">
                <a href="/index.php?route=account/notification"><?php echo $heading_spov; ?>
                  <?php if($count) {?>
                  <div class="notif">
                    <?=$count?>
                  </div>
                  <?php } ?>
                </a>
            </li>
            <li role="presentation" >
                <a href="/index.php?route=account/coupon"><?php echo $heading_kyp; ?></a>
            </li>
        </ul>
      <h3 class="text-center" style=" text-transform: uppercase;">

          <?php echo $heading_spov; ?>

      </h3>
      <div class="col-sm-12">
        <?php foreach($notifications as $notification) {?>

        <div class="item_notification <?= (!$notification['viewed'])?'new':''; ?> ">
           <p>
           <?php if(!$notification['viewed']){?>
             <i class="pull-right">New</i>
           <?php } ?>
             <?php echo $status; ?> <b><?=$notification['order']?></b> <?php echo $vid; ?> <b><?=$notification['date_added']?></b><br>
        <?php echo $zmin; ?> : &nbsp<b>
        <?php if($notification['status'] == 'В обробці'){ ?>
                <?= $decoration; ?>
        <?php }elseif($notification['status'] == 'Відправлено'){ ?>
          <?= $posted; ?>
          <?php }elseif($notification['status'] == 'Доставлено'){ ?>
          <?= $delivered; ?>
          <?php }elseif($notification['status'] == 'Оформлено'){ ?>
          <?= $decorated; ?>
          <?php } ?>
        </b>
           </p>
           <p>
             <?php if(strlen($notification['comment'])>0){?>
             <?php echo $com; ?> :&nbsp <?=$notification['comment'] ?>
             <?php }else{ ?>
             <?php echo $comvid; ?>
             <?php } ?>
           </p>

           <i class="pull-right"><?=$notification['date']?></i>
           <div style="clear:both;"> </div>

        </div>
        <?php } ?>
      <?php echo $content_bottom; ?></div>
      <div class="clearfix"></div>
    <?php echo $column_right; ?>
  </div>
  </div>
</div>
</div>
<?php echo $footer; ?>
