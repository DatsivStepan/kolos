<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8 col-md-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="prof">
            <ul class="nav nav-tabs">
                <li role="presentation">
                    <a href="../../../index.php?route=account/edit"><?php echo $heading_title1; ?></a>
                </li>
                <li role="presentation" class="active">
                    <a  href="../../../index.php?route=account/order"><?php echo $heading_zam; ?></a>
                </li>
                <li role="presentation">
                    <a href="/index.php?route=account/notification"><?php echo $heading_spov; ?>
                      <?php if($notifications_count) {?>
                      <div class="notif">
                        <?=$notifications_count?>
                      </div>
                      <?php } ?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="/index.php?route=account/coupon"><?php echo $heading_kyp; ?></a>
                </li>
            </ul>
        <h3 class="text-center" style=" text-transform: uppercase;">

            <?php echo $heading_zam; ?>

        </h3>
        <?php if ($orders) { ?>
      <div class="table-responsive">
        <!--<table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right"><?php echo $column_order_id; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-right"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_customer; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td></td>
            </tr>
          </thead>
          <tbody>-->
            <?php foreach ($orders as $order) { ?>
                        <div id="orderModal<?= $order['order_id']; ?>" class="modal fade" role="dialog">
                            <div class="modal-dialog" style="width:80%;">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                    <div class="col-sm-3">
                                        <?php echo $heading_zam; ?>: <?= $order['order_id']; ?>
                                    </div>
                                    <div class="col-sm-3" style='text-align: right'>
                                        <?php echo $status; ?>: <span style='color:#f2cd3a;'>
                                                <?php if($order['status'] == 'В обробці'){ ?>
                                                <?= $decoration; ?>
                                                <?php }elseif($order['status'] == 'Відправлено'){ ?>
                                                <?= $posted; ?>
                                                <?php }elseif($order['status'] == 'Доставлено'){ ?>
                                                <?= $delivered; ?>
                                                <?php }elseif($order['status'] == 'Оформлено'){ ?>
                                                <?= $decorated; ?>
                                                <?php } ?></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" class="close" data-dismiss="modal"  style='background-color: transparent;border:1px solid #fd4242;color:#fd4242;border-radius:40px;padding:5px 20px;font-size:12px;'><?php echo $close; ?> &times;</button>
                                    </div>
                                    <div style='clear:both;'></div>
                                </div>
                                <div class="modal-body">
                                    <div class="col-sm-8">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td><?php echo $tovar; ?></td>
                                                    <td><?php echo $kilkist; ?></td>
                                                    <td><?php echo $cina; ?></td>
                                                    <td><?php echo $vartist; ?></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($order['products_list'] as $product){ ?>
                                                    <tr>
                                                        <td><?= $product['name']; ?></td>
                                                        <td><?= $product['quantity']; ?></td>
                                                        <td><?= $product['price']; ?></td>
                                                        <td><?= $product['total']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-4">
                                        <table class="table table-bordered table-hover">
                                            <tr>
                                                <td>
                                                    <?php echo $osob; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        switch($order['special_notes']){
                                                            case 1:
                                                               echo $bad1;
                                                            break;
                                                            case 2:
                                                               echo $bad2;
                                                            break;
                                                            case 3:
                                                               echo $bad3;
                                                            break;
                                                            case 4:
                                                               echo $bad4;
                                                            break;
                                                            case 5:
                                                               echo $bad5;
                                                            break;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $iak; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        switch($order['referral']){
                                                            case 1:
                                                               echo $iak1;
                                                            break;
                                                            case 2:
                                                               echo $iak2;
                                                            break;
                                                            case 3:
                                                               echo $iak3;
                                                            break;
                                                            case 4:
                                                               echo $iak4;
                                                            break;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $kilkistt; ?>
                                                </td>
                                                <td>
                                                    <?= $order['products']; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $kom; ?>
                                                </td>
                                                <td>
                                                    <?= $order['o_commission']; ?> грн.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $kilkistpos; ?>
                                                </td>
                                                <td>
                                                    <?php  
                                                    $parcel = json_decode($order['parcel']);
                                                    if($parcel){
                                                        foreach($parcel as $count => $price){
                                                            echo $count.' - '.$price.'<br>';
                                                        }
                                                    }else{
                                                        echo 1;
                                                    } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $zagvat; ?>
                                                </td>
                                                <td>
                                                    <?= $order['total_price_with_commission']; ?>
                                                    <?= $order['total_price_with_commission_usd']; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style='clear:both;'></div>
                                </div>
                              </div>

                            </div>
                        </div>

            <div style="padding:10px;border:1px solid #e5e5e5;margin-top:20px;background-color: #f8f8f8">
                <div class='col-sm-12'>
                    <div class="col-sm-3">
                        <?php echo $heading_zam; ?>: <?= $order['order_id']; ?>
                    </div>
                    <div class="col-sm-3 text-left">
                        <?php echo $status; ?>: <span style='color:#f2cd3a;'>
                            <?php if($order['status'] == 'В обробці'){ ?>
                            <?= $decoration; ?>
                            <?php }elseif($order['status'] == 'Відправлено'){ ?>
                            <?= $posted; ?>
                            <?php }elseif($order['status'] == 'Доставлено'){ ?>
                            <?= $delivered; ?>
                            <?php }elseif($order['status'] == 'Оформлено'){ ?>
                            <?= $decorated; ?>
                            <?php } ?></span>
                    </div>
                    <div class="col-sm-3" style='text-align: right'>
                        <button data-order_id="<?= $order['order_id']; ?>" class='btn orderShowModal' style='background-color: transparent;border:1px solid #fd4242;color:#fd4242;border-radius:40px;'><?php echo $show_order; ?></button>
                        <!--<a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class='btn' style='background-color: transparent;border:1px solid #fd4242;color:#fd4242;border-radius:40px;'>Переглянути заказ</a> -->
                    </div>
                    <div class="col-sm-3" style='text-align: right'>
                        <span class="showDetailOrder" data-show="hide" style='color:#fd4242;cursor:pointer;'><?php echo $more; ?> <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class='col-sm-12 blockMoreInfo' style="display:none;">
                    <div class='col-sm-12'>
                        <?php if($order['status'] == 'Оформлено'){ ?>
                            <img src='../../../../image/order_slider/slider_status_2.png' style='margin-top:15px;width:100%;'>
                        <?php }elseif($order['status'] == 'В обробці'){ ?>
                            <img src='../../../../image/order_slider/slider_status_3.png' style='margin-top:15px;width:100%;'>
                        <?php }elseif($order['status'] == 'Відправлено'){ ?>
                            <img src='../../../../image/order_slider/slider_status_4.png' style='margin-top:15px;width:100%;'>
                        <?php }elseif($order['status'] == 'Доставлено'){ ?>
                            <img src='../../../../image/order_slider/slider_status_5.png' style='margin-top:15px;width:100%;'>
                        <?php } ?>
                    </div>
                    <div class='col-sm-12' style='padding:0px;'>
                        <div class='col-sm-3 '><?php echo $decorated; ?></div>
                        <div class='col-sm-3 text-center'><?php echo $decoration; ?></div>
                        <div class='col-sm-3 text-right'><?php echo $posted; ?></div>
                        <div class='col-sm-3 text-right'><?php echo $delivered; ?></div>
                    </div>
                </div>
                <div style='clear:both;'></div>
            </div>
            <?php } ?>
          <!--</tbody>
                <tr>
                  <td class="text-right">#<?php echo $order['order_id']; ?></td>
                  <td class="text-left"><?php echo $order['status']; ?></td>
                  <td class="text-left"><?php echo $order['date_added']; ?></td>
                  <td class="text-right"><?php echo $order['products']; ?></td>
                  <td class="text-left"><?php echo $order['name']; ?></td>
                  <td class="text-right"><?php echo $order['total']; ?></td>
                  <td class="text-right"><a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a></td>
                </tr>
        </table> -->
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <div class="col-sm-12">
            <p><?php echo $text_empty; ?></p>
      </div>
      <?php } ?>
      <!--<div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div> -->
      <?php echo $content_bottom; ?>
  </div>
  </div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $footer; ?>
