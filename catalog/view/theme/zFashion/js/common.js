var count_show = 1;
var tmp_count = 1;
function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

$(document).ready(function() {
	
		$(document).on('click','.clickCategoryName',function(){
			document.location.replace($(this).data('href'));
		});
//        $('.minusQuantityCount)
		$(document).on('click','.minusQuantityCount',function(){
			var quantity_input = $(this).parent().find('.input-quantity');
			var quantityValue = quantity_input.val();
			if(quantityValue <= 1){
				quantity_input.val('1');
			}else{
				var valueQ = parseInt(quantityValue) - 1;
				quantity_input.val(valueQ);
			}
		});
		
		$(document).on('click','.plusQuantityCount',function(){
			var quantity_input = $(this).parent().find('.input-quantity');
			var quantityValue = quantity_input.val();
				var valueQ = parseInt(quantityValue) + 1;
				quantity_input.val(valueQ);
		});
	// Adding the clear Fix
	cols1 = $('#column-right, #column-left').length;
	
	// if (cols1 == 2) {
	// 	$('#content .product-layout:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
	// } else if (cols1 == 1) {
	// 	$('#content .product-layout:nth-child(4n+4)').after('<div class="clearfix visible-lg"></div>');
	// } else {
	// 	$('#content .product-layout:nth-child(4n+4)').after('<div class="clearfix"></div>');
	// }
	
	$('.clickParentMenu').on('click', function(){
		$(this).parent().find('.dropDownSubMenu').toggle();
		
		if($(this).find('.arrow-icon').hasClass('fa-angle-up')){
			$(this).find('.arrow-icon').addClass('fa-angle-down');
			$(this).find('.arrow-icon').removeClass('fa-angle-up');
		}else{
			$(this).find('.arrow-icon').addClass('fa-angle-up');
			$(this).find('.arrow-icon').removeClass('fa-angle-down');
		}
	});
		
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();
		
		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});
		
	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
		url = $('base').attr('href') + 'index.php?route=product/search';
		doc_w = $(document).width();
if (doc_w<1023){
	var value = $('#searchm input[name=\'search\']').val();

}else{
	var value = $('header input[name=\'search\']').val();
}

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-layout').attr('class', 'product-layout product-list col-xs-12');
		
		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		$('#content .product-layout > .clearfix').remove();

		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-6');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});


	var add = document.getElementById('add').title;
	if (add == 'Українська') {
		add = 'Добавлено';
	}
	if (add == 'russian') {
		add = 'Добавлено';
	}
	if (add == 'English') {
		add = 'Added';
	}
	if($(".added_product_id")) {
		$(".added_product_id").each(function (index, elem) {
			$(".product-id-" + $(elem).val()).addClass('product_added');
			$(".product-id-" + $(elem).val() + " button[type='button']").addClass('added_to_cart').text(add);
			//alert($(elem).val());
		})
	}
});

function integerDivision(x, y){
	return (x-x%y)/y
}
// Cart add remove functions
var cart = {
'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},			
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}
				var add = document.getElementById('add').title;
						if (add == 'Українська') {
					add = 'Добавлено';
                var labelshow1 = 'Перейти в кошик';
                var labelshow2 = 'Продовжити покупку';    
				}
				if (add == 'russian') {
					add = 'Добавлено';
                    var labelshow1 = 'Перейти в корзину';
                var labelshow2 = 'Продолжить покупку';
				}
				if (add == 'English') {
					add = 'Added';
                    var labelshow1 = 'Go to basket';
                var labelshow2 = 'Continue Shopping';
				}
				$(".product-id-"+product_id).addClass('product_added');
				$(".product-id-" + product_id + " button[type='button']").addClass('added_to_cart').text(add);

				if (json['success']) {
									var d = new Date();
									var n = d.getTime();
										if(parseInt(json['total']) > 0){
											$('.CartImageHeader').attr('src','../../../../image/mishok_cart.png');
											$('.CartImageHeader').css('padding-right','0px');
											$('.CartImageHeader').css('padding-left','0px');
										}else{
											$('.CartImageHeader').attr('src','../../../../image/mishok.png');
											$('.CartImageHeader').css('padding-right','14px');
											$('.CartImageHeader').css('padding-left','7px');
										}
										
										console.log('weight', json['weight']);
										var post_count =  (integerDivision(json['weight'], 30) == 0)?1:integerDivision(json['weight'], 30);
										var tmp = 1*post_count+1;
										var add_tmp_count = 1;
										if(json['weight'] > 30){
											add_tmp_count = integerDivision(json['weight'], 30)
											add_tmp_count = add_tmp_count + 1;
										}else{
											add_tmp_count = 1;
										}
										
											console.log(tmp_count);
											console.log(add_tmp_count);
											
											if(tmp_count < add_tmp_count){
												tmp_count = add_tmp_count;
												if(json['weight'] >= 30*post_count){
													bootbox.dialog({
														message: json['text1']+' '+ (tmp-1)*30 + ''+json['text2']+'' + tmp + ' '+json['text3'],
															buttons: {
																	success: {
																			label: labelshow1,
																			className: "btn-to-cart",
																			callback: function() {
																					window.location.assign("/index.php?route=checkout/cart");
																			}
																	},
																	main: {
																			label: labelshow2,
																			className: "btn-to-shop",
																	}
															}
													});
												}
											}
											
											
											
					setTimeout(function () {
						$('#cartt').html(parseInt(json['total']));
					}, 100);

					//$('html, body').animate({ scrollTop: 0 }, 'slow');
					var beepOne = $("#beep-one")[0];
					beepOne.play();

					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},			
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
								if(parseInt(json['total']) > 0){
									$('.CartImageHeader').attr('src','../../../../image/mishok_cart.png');
									$('.CartImageHeader').css('padding-right','0px');
									$('.CartImageHeader').css('padding-left','0px');
								}else{
									$('.CartImageHeader').attr('src','../../../../image/mishok.png');
									$('.CartImageHeader').css('padding-right','14px');
									$('.CartImageHeader').css('padding-left','7px');
								}
				setTimeout(function () {
					$('#cartt').html(parseInt(json['total']));
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	},
	'remove': function(key, product_id) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},			
			success: function(json) {
								console.log(json);
								if(parseInt(json['total']) > 0){
									$('.CartImageHeader').attr('src','../../../../image/mishok_cart.png');
									$('.CartImageHeader').css('padding-right','0px');
									$('.CartImageHeader').css('padding-left','0px');
								}else{
									$('.CartImageHeader').attr('src','../../../../image/mishok.png');
									$('.CartImageHeader').css('padding-right','14px');
									$('.CartImageHeader').css('padding-left','7px');
								}
								
								var post_count =  (integerDivision(json['weight'], 30) == 0)?1:integerDivision(json['weight'], 30);
								var tmp = 1*post_count+1;

								if(json['weight'] > 30){
									tmp_count = integerDivision(json['weight'], 30)
									tmp_count = tmp_count + 1;
									count_show = tmp_count;
								}else{
									tmp_count = 1;
									count_show = 1
								}
								
				if(product_id != null){
					var add = document.getElementById('add').title;
					if (add == 'Українська') {
						add = 'Добавлено';
					}
					if (add == 'russian') {
						add = 'Добавлено';
					}
					if (add == 'English') {
						add = 'Added';
					}
					$(".product-id-"+product_id).removeClass('product_added');
					$(".product-id-" + product_id + " button[type='button']").removeClass('added_to_cart').text(add);
				}
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cartt').html(parseInt(json['total']));
				}, 100);
					
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				if (json['info']) {
					$('#content').parent().before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#wishlist-total span').html(json['total']);
				$('#wishlist-total').attr('title', json['total']);

				//$('html, body').animate({ scrollTop: 0 }, 'slow');
				var beepOne = $("#beep-one")[0];
				beepOne.play();
			}
		});
	},
	'remove': function() {

	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					//$('html, body').animate({ scrollTop: 0 }, 'slow');
					var beepOne = $("#beep-one")[0];
					beepOne.play();
				}
			}
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();
	
			$.extend(this, option);
	
			$(this).attr('autocomplete', 'off');
			
			// Focus
			$(this).on('focus', function() {
				this.request();
			});
			
			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);				
			});
			
			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}				
			});
			
			// Click
			this.click = function(event) {
				event.preventDefault();
	
				value = $(event.target).parent().attr('data-value');
	
				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}
			
			// Show
			this.show = function() {
				var pos = $(this).position();
	
				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});
	
				$(this).siblings('ul.dropdown-menu').show();
			}
			
			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}		
			
			// Request
			this.request = function() {
				clearTimeout(this.timer);
		
				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}
			
			// Response
			this.response = function(json) {
				html = '';
	
				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}
	
					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}
	
					// Get all the ones with a categories
					var category = new Array();
	
					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}
	
							category[json[i]['category']]['item'].push(json[i]);
						}
					}
	
					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
	
						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}
	
				if (html) {
					this.show();
				} else {
					this.hide();
				}
	
				$(this).siblings('ul.dropdown-menu').html(html);
			}
			
			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));	
			
		});
	}
})(window.jQuery);

// CATEGORY MENU
$(document).ready(function() {

	$('#namemore').on('click', function(){
		$(this).toggleClass('stick');
	})

	$('.sticky-menu').bind("DOMSubtreeModified", function(){
		footerPosition = $('.footer').offset().top - $(window).scrollTop() - 135;
		if($('.sticky-menu').height() >= footerPosition ){
			$(this).css({
				'max-height': $('.footer').offset().top - $(window).scrollTop()
			});
		}
	})
	$('.customdrop li').on('click', function(){
		if ($('.dropDownSubMenu').is(":visible")){
			footerPosition = $('.footer').offset().top - $(window).scrollTop() - 135;
			if($('.sticky-menu').height() >= footerPosition ){
				$('.sticky-menu').css({
					'max-height': $('.footer').offset().top - $(window).scrollTop()
				});
			}
			console.log(footerPosition);
		}
	})

	$(window).on('scroll , resize, load', function() {

		// desktop sticky menu

		var columnLeft = $('#column-left').innerWidth(),
			topMenuWrap = $('#column-left').offset().top - $(window).scrollTop(),
			footerPosition = $('.footer').offset().top - $(window).scrollTop() - 50,
			wHeight = window.innerHeight;

			if(topMenuWrap <=0 && window.innerWidth >= 768) {
				$('.sticky-menu').addClass('stickytop');
				$('.stickytop').css({
					'width': columnLeft,
					'max-height': wHeight,
					'overflow-y': 'auto'
				});

				if($('.sticky-menu').height() >= footerPosition ){
					$('.sticky-menu').css({
						'max-height': $('.footer').offset().top - $(window).scrollTop()
					});
				}
			}
			else{
				$('.sticky-menu').removeClass('stickytop');
				$('.sticky-menu').attr('style', '');
			}
	});
})
