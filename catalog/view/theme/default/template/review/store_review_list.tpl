<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="rewiew">
  <div class="col-sm-3">
    <div class="avatar-row text-center"><img src="image/vidgyk.png" class="kolo" style="    width: 40%;" alt=""></div>
    <div class="text-center"><strong><?php echo $review['author']; ?></strong></div>

  </div>
  <div class="col-sm-9 rewiew-row">
    <p style="    padding-bottom: 5%; word-wrap: break-word;"><?php echo $review['text']; ?></p>
    <div class="row">
      <div class="col-sm-8 text-left star-rating">
        <?php for ($i = 1; $i <= 5; $i++) { ?>
        <?php if ($review['rating'] < $i) { ?>
        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x" style='color: #FC0;'></i></span>
        <?php } else { ?>
        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x" style='color: #FC0;'></i><i class="fa fa-star-o fa-stack-2x" style='color: #E69500;'></i></span>
        <?php } ?>
        <?php } ?>
      </div>
      <div class="col-sm-4 text-right"><?php echo $review['date_added']; ?></div>

    </div>
  </div>
  <div class="clearfix"></div>
</div>

<?php } ?>
<div class="">
  <div class="col-sm-6 text-left count-product"><?php echo $results; ?></div>
  <div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
  <div class="clearfix"></div>
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
