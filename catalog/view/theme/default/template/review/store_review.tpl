<?php echo $header; ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-8 col-md-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
            <form class="form-horizontal" id="form-review">
                <h1 class="text-center reviews-header">

                    <?php echo $heading_title; ?>

                </h1>
                
                <?php if ($review_status) { ?>
                <div id="review"></div>
                <?php if ($review_guest) { ?>

                    <div class="col-sm-12" style="padding:0px;">
                        <div class="col-sm-4 required">
<div class="row">
                            <input placeholder="<?php echo $entry_name; ?>" style="width: 100%;background-color: #f8f8f8;" type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control"/>
</div>
                            <div class="row" style="margin-top: 5px;">
                            <input placeholder="<?php echo $entry_mail; ?>" style="width: 100%;background-color: #f8f8f8;" type="text" name="mail" value="<?php echo $customer_mail; ?>" id="input-mail" class="form-control"/>
</div>

                            <div class="row">

                            <div class="col-sm-12 no-padding review-block" >
                                <div class="pull-left">
                                    <label class="control-label"><?php echo $entry_rating; ?></label>
                                </div>
                                <div id="reviewStars-input">
                                    <input id="star-4" type="radio" name="rating"  value="5"/>
                                    <label title="gorgeous" for="star-4"></label>

                                    <input id="star-3" type="radio" name="rating"  value="4"/>
                                    <label title="good" for="star-3"></label>

                                    <input id="star-2" type="radio" name="rating"  value="3"/>
                                    <label title="regular" for="star-2"></label>

                                    <input id="star-1" type="radio" name="rating"  value="2"/>
                                    <label title="poor" for="star-1"></label>

                                    <input id="star-0" type="radio" name="rating"  value="1"/>
                                    <label title="bad" for="star-0"></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            </div>
                        </div>


                        <div class="col-sm-8 required review-textarea-wrap">
                            <textarea placeholder="<?php echo $entry_review; ?>" name="text" rows="5" id="input-review" class="form-control"></textarea>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                <div class="form-group required">

                <?php if (isset($site_key) && $site_key) { ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                    </div>
                </div>
                <?php } elseif(isset($captcha) && $captcha){ ?>
                <?php echo $captcha; ?>
                <?php } ?>
                <div class="buttons clearfix">
                    <div style="text-align:center;">
                        <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>"
                                class="btn" style="background-color: #ff0000;color:white;padding:10px 30px;border-radius:40px;border:0px;"> <?php echo $send; ?> </button>
                    </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
                <?php } ?>
            </form>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>

    <script type="text/javascript">
        $('#review').delegate('.pagination a', 'click', function (e) {
            e.preventDefault();
            $('#review').load(this.href);
        });

        $('#review').load('index.php?route=review/store_review/review');

        $('#button-review').on('click', function () {
            console.log($("#form-review").serialize());
            $.ajax({
                url: 'index.php?route=review/store_review/write',
                type: 'post',
                dataType: 'json',
                data:  $("#form-review").serialize(),
                beforeSend: function () {
                    if ($("textarea").is("#g-recaptcha-response")) {
                        grecaptcha.reset();
                    }
                    $('#button-review').button('loading');
                },
                complete: function () {
                    $('#button-review').button('reset');
                },
                success: function (json) {
                    $('.alert-success, .alert-danger').remove();
                    if (json['error']) {
                        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }
                    if (json['success']) {
                        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                        $('input[name=\'reviewStars\']:checked').prop('checked', false);
                    }
                }
            });
        });
       </script>
</div>
</div>
<?php echo $footer; ?>
