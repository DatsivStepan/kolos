<div class="panel panel-default">
    <div class="panel-heading"><b> Каталог товаров </b><!--- <?php echo $heading_title; ?> --> </div>    


<ul class="customdrop" role="menu" aria-labelledby="dropdownMenu">


<?php foreach ($categories as $category) { ?>
    <?php 
        $classes = '';
        if ($category['category_id'] == $category_id) { 
            $classes .= 'active';
        }

        if($category['children']) {
            $classes .= ' dropdown-submenu';
        }
    ?>
        <li class="<?php echo $classes; ?>">
            <a href="<?php echo $category['href'];?>" title="<?php echo $category['name'];?>">
                <?php echo $category['name']; ?>
            </a>

            <?php if($category['children']) { ?>
            <ul class="dropdown-menu">
                <?php 
                    $classes = '';
                    foreach ($category['children'] as $child)
                    { 
                        if($child['children']) {
                            $classes .= ' dropdown-submenu';
                        }
                ?>
                <li class="<?php echo $classes; ?>">
                    <a href="<?php echo $child['href'];?>" tabindex="-1" title="<?php echo $child['name'];?>"><?php echo $child['name'];?></a>
                    <?php 
                        if($child['children']) {
                    ?>
                    <ul class="dropdown-menu">
                        <?php 
                            $classes = '';
                            foreach ($child['children'] as $subChild)
                            { 
                                if($subChild['children']) {
                                    $classes .= 'dropdown-submenu';
                                }
                        ?>
                        <li class="<?php echo $classes; ?>">
                            <a href="<?php echo $subChild['href'];?>" title="<?php echo $subChild['name'];?>"><?php echo $subChild['name'];?></a>
                        </li>
                        <?php } ?>
                    </ul>
                        <?php } ?>
                </li>
                <?php } ?>
            </ul>

                <?php }?>

        </li>
<?php } ?>
    </ul>

</div>