</div>
<div class="clearfix"></div>
<div class="container">
	<div class="row">
		<div class="col-sm-offset-4 col-md-offset-3 col-sm-8 col-md-9">
			<h3 class="popular-header text-center">
				<?= $heading_title; ?>
			</h3>
			<div class="container-fluid">
				<div class="row popular-row">
					<?php $q=0; foreach ($products as $product) { $q++;  ?>
					<?php if($q%6==5){ ?>
<div id="clear" style=""></div>
                <script>
                    if (screen.width > 960) {
    document.getElementById("clear").style = "clear: both;";
}
else {

    document.getElementById("clear").style = "";
}
                
                    
                </script>
					<?php } ?>
						<div class="product-id-<?=$product['product_id'];?> fiveparts">
						<?php if ($product['special']) { ?>
					<span class="ribbon">HOT<b></b></span>
				<?php } ?>
							<div class="product-thumb transition">
								<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
								<div class="image">
									<a href="<?php echo $product['href']; ?>">
										<img src="<?php echo $product['thumb']; ?>"
											alt="<?php echo $product['name']; ?>"
											title="<?php echo $product['name']; ?>"
											class="img-responsive"/>
									</a>
								</div>
								<div class="caption">
									<div class="row no-margin text-center">
										<div class="col-xs-12">
											<?php if ($product['price']) { ?>
											<p class="price">
												<?php if (!$product['special']) { ?>
												<?php echo $product['price']; ?>
												<?php } else { ?>
												<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
												<?php } ?>
											</p>
											<?php } ?>
										</div>
										<div class="col-xs-12">
											<i class="charges fa fa-minus minusQuantityCount"></i>
											<input type="text" name="quantity" value="<?=$product['minimum']?>" size="1"  class="text-center input-quantity" />
											<i class="charges fa fa-plus plusQuantityCount"></i>
										</div>
									</div>
									<div class="row add-block">
										<div class="purse"></div>
										<button type="button" class="btn button_cart" onclick="cart.add('<?php echo $product['product_id']; ?>', $('.product-id-<?=$product['product_id'];?>').find('.input-quantity').val())" > <?php echo $button_cart; ?></button>
									</div>
								</div>
							</div>
						</div>
					<?php }  ?>
				</div>
			</div>
		</div>
		
	</div>
</div>
