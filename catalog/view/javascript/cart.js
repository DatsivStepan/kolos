var update_cart = function (key, quantity, id) {
    
    console.log('update key: ', key);
    console.log('update quantity: ', quantity);
    $.ajax({
        url: 'index.php?route=checkout/cart/ajaxEdit',
        type: 'post',
        data: {
            quantity: (typeof(quantity) != 'undefined' ? quantity : 1),
            key: key
        },

        //dataType: 'json',
        beforeSend: function () {
            console.log('beforeSend');
            //$('#cart > button').button('loading');
        },
        complete: function () {
            console.log('complete');
            //$('#cart > button').button('reset');
        },
        success: function (json) {
            var tp = json.totals;
            $("#totalPrice").html(tp[tp.length-1].text);
            $("#totalWeight").html(json.weight);
            $("#cartCount").html(json.count);
            $("#parcel").html(json.parcel);
            $("#pw_" + id).html(json.p_w);
            console.log(json.p_w);
            $("#commission").html(json.commission+' $');
            $("#allTotalPrice").html(json.totalPrice);
             $('#cartt').html(json.count);
            var beepOne = $("#beep-one")[0];
					beepOne.play();

					$('#cart > ul').load('index.php?route=common/cart/info ul li');


        }
    });
    /* $.ajax({
     url: "/index.php?route=checkout/cart/edit",
     type: "POST",
     data: {'quantity[YToxOntzOjEwOiJwcm9kdWN0X2lkIjtpOjYwO30=]' : 1}
     }).done(function(msg) {
     alert(msg);
     }).fail(function(jqXHR, textStatus) {
     alert( "Request failed: " + textStatus );
     });*/
};

function quantity_dec(key, id) {
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val();
    console.log(q_count);
    var q = 1;
    if(q_count > 1){
        q = parseInt(q_count) - 1;
        update_cart(key, q, id);
        q_obj.val(q)
    }




}
function quantity_inc(key, id) {
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val(),
        q = parseInt(q_count) + 1;

    update_cart(key, q, id);
    q_obj.val(q)
}

function quantity_onchange(key, id) {
    
    var q_obj = $("input[name='quantity[" + key + "]']");
    
    
    var q_count = q_obj.val();
        if(q_count==''){q_count=0;}
    console.log('q_obj key: ', q_count);
        q = parseInt(q_count);
    update_cart(key, q, id);
    q_obj.val(q)
}

