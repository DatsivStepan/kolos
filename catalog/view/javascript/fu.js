$(document).ready(function () {
    
    var previewNode = document.querySelector(".previewTemplateQ");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);
    var myDropzone = new Dropzone($('#fileUpload')[0], {
        maxFiles:1,
        uploadMultiple:false,
        url: "/index.php?route=account/edit/photo",
        previewTemplate:previewTemplate,
        clickable: "#fileUpload",
        previewsContainer: ".post_container_photo",
    });
  
    myDropzone.on("complete", function(response) {
        if (response.status == 'success') {
            if(response.xhr.response != 'false'){
                $('#fileUpload').css("background-image", "url('../../../image/catalog/avatars/"+response.xhr.response+"')");
                $('.photoProfileSrc').val(response.xhr.response);
            }
        }else{
            console.log('false');
        }
    });
  
})
